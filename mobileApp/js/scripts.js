$(document).ready(function(){

  var DateUtil= {

    getDaysAroundThreeMonths : function(){
      let momentToday=moment(new Date());
      let startDate = moment(new Date()).subtract(3, 'months');
      let endDate   = moment(new Date()).add(3, 'months');;
      let now = startDate.clone();
      let monthDates={
        today : {
                  dayWord : DateUtil.shortDay(momentToday.format('dddd')),
                  dayNo : momentToday.date(),
                  month : DateUtil.shortMonth(momentToday.format('MMMM')),
                  year : momentToday.year()
                },
        dates : []
      };
        while (now.isSameOrBefore(endDate)) {
            monthDates.dates.push({
              dayWord : DateUtil.shortDay(now.format('dddd')),
              dayNo : now.date(),
              month : DateUtil.shortMonth(now.format('MMMM')),
              year : now.year()
            });
            now.add(1, 'days');
        }
        return monthDates;
    },

    shortDay: function(day){
      return day.substring(0,3).toUpperCase();
    },

    shortMonth: function(month){
      return month[0].toUpperCase() + month.substring(1,3);
    }
  };

  var SocialShareOptions = {
      message: 'Hi Mate! I am using ForYou , a new subscription service. They deliver everything from milk, bread, fruits & vegrtables, diapers, staples and so much more right to my doorstep. I get fresh milk and fresh produce everything morning without having to worry about putting milk coupons out every night. Check them out here.', // not supported on some apps (Facebook, Instagram)
      files: ['', ''], // an array of filenames either locally or remotely
      url: 'https://play.google.com/store/apps/details?id=com.bigbasket.mobileapp&hl=en_IN&gl=US',
      chooserTitle: 'Share App' // Android only, you can override the default share sheet title
    };

  var service={

    buildHomeCategories:function(){
      $.ajax({
        type: 'POST',
        url: "http://localhost/foru/product/categories",
        contentType:"application/json",
        success: function(resultData) {
          for(i in resultData){
            let category=resultData[i].id;
            let name=resultData[i].name;
            let image=resultData[i].imageLocation;
            $('#categories').append('<div class="col-4 category-item flex-fill" data-category="'+category+'">'+
                                      '<div class="col-12">'+
                                        '<img class="mx-auto d-block img-thumbnail" src="'+image+'">'+
                                      '</div>'+
                                      '<div class="col-12 text-center p-0">'+name+'</div>'+
                                  '</div>');
          }
        },
        error: function(resultData) {
          $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
             $("#ajax-error-alert").slideUp(500);
           });
         }
      });
    },

    buildPromotions:function(){
      $.ajax({
        type: 'POST',
        url: "http://localhost/foru/promotions/fetch",
        contentType:"application/json",
        success: function(resultData) {
          for(i in resultData){
            let elem;
            switch (resultData[i].promotionGroup) {
              case 'HOME_FIRST_CAROUSEL':
                  elem='<div class="carousel-item" data-is-info="'+resultData[i].isInfo+'" data-is-category="'+resultData[i].isCategory+'" data-is-product="'+resultData[i].isProduct+'" data-category="'+resultData[i].categoryId+'" data-product="'+resultData[i].productId+'">'+
                             '<img src="'+resultData[i].imageLocation+'" class="d-block w-100" alt="...">'+
                           '</div>';
                           $('#home-promotions-1').find('.carousel').find('.carousel-inner').append(elem);
                break;
              case 'HOME_SECOND_CAROUSEL':
               elem='<div class="carousel-item" data-is-info="'+resultData[i].isInfo+'" data-is-category="'+resultData[i].isCategory+'" data-is-product="'+resultData[i].isProduct+'" data-category="'+resultData[i].categoryId+'" data-product="'+resultData[i].productId+'">'+
                          '<img src="'+resultData[i].imageLocation+'" class="d-block w-100" alt="...">'+
                        '</div>';
                        $('#home-promotions-2').find('.carousel').find('.carousel-inner').append(elem);
                break;
              case 'HOME_BOTTOM':
                 elem='<div class="mt-2 p-2 text-center home-bottom-promotion" data-is-info="'+resultData[i].isInfo+'" data-is-category="'+resultData[i].isCategory+'" data-is-product="'+resultData[i].isProduct+'" data-category="'+resultData[i].categoryId+'" data-product="'+resultData[i].productId+'">'+
                            '<img src="'+resultData[i].imageLocation+'" class="d-block w-100" alt="...">'+
                        '</div>;'
                $('.home-tab-cart-footer').before(elem);
                break;
            }
          }
          $('#home-promotions-1').find('.carousel').find('.carousel-inner').find('.carousel-item').eq(0).addClass('active');
          $('#home-promotions-2').find('.carousel').find('.carousel-inner').find('.carousel-item').eq(0).addClass('active');
        },
        error: function(resultData) {
          $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
             $("#ajax-error-alert").slideUp(500);
           });
         }
      });
    },

    buildHomeMilk:function(){
      $.ajax({
        type: 'POST',
        url: "http://localhost/foru/product/category-products",
        contentType:"application/json",
        data :JSON.stringify({category : 'Milk'}),
        success: function(resultData) {
          let products=resultData.products
          for(i in products){
            let elem='<div class="col-6 mr-2 product-item" data-sku-id="'+products[i].id+'">'+
                        '<div class="col-12">'+
                          '<img class="mx-auto d-block img-thumbnail" src="'+products[i].imageLocation+'">'+
                        '</div>'+
                        '<div class="mb-1 row col-12 light">'+products[i].brand+'</div>'+
                        '<div class="mb-1 row col-12 dark">'+products[i].name+'</div>'+
                        '<div class="mb-1 row col-12 light">'+products[i].measurement+'</div>'+
                        '<div class="mb-1 row col-12 dark">₹'+products[i].currentPrice+'</div>';

              if(products[i].subscriptionAvailability){
                elem=elem+'<div class="mb-1 row col-12 product-item-btns-holder">'+
                            '<button type="button" class="btn btn-success btn-subscribe">Subscribe @ ₹'+products[i].subscriptionPrice+'</button>'+
                          '</div>';
              }

              if(products[i].availableQuantity > 0){
                elem=elem+'<div class="row col-12 product-item-btns-holder">'+
                            '<button type="button" class="btn btn-light btn-buy-once">Buy Once</button>'+
                          '</div>'+
                          '<div class="col-12 text-center buy-once-prod-quantity-control just-border">'+
                            '<i class="fas fa-minus-circle ml-2 p-2"></i>'+
                            '<span class="count">1</span>'+
                            '<i class="fas fa-plus-circle  ml-2 p-2"></i>'+
                          '</div>';
              }
              else{
                elem=elem+'<div class="mb-1 row col-12">'+
                            '<button type="button" class="btn w-100 out-of-stock">Out of Stock</button>'+
                          '</div>';
              }
              elem=elem+'</div>';
             $('#all-mliks').append(elem);
          }
          $('.buy-once-prod-quantity-control').hide();
        },
        error: function(resultData) {
          $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
             $("#ajax-error-alert").slideUp(500);
           });
         }
      });
    },

    buildDeals:function(){
      $.ajax({
        type: 'POST',
        url: "http://localhost/foru/promotions/deals",
        contentType:"application/json",
        success: function(products) {
          for(i in products){
            let elem='<div class="col-6 mr-2 product-item" data-sku-id="'+products[i].id+'">'+
                        '<div class="col-12">'+
                          '<img class="mx-auto d-block img-thumbnail" src="'+products[i].imageLocation+'">'+
                        '</div>'+
                        '<div class="mb-1 row col-12 light">'+products[i].brand+'</div>'+
                        '<div class="mb-1 row col-12 dark">'+products[i].name+'</div>'+
                        '<div class="mb-1 row col-12 light">'+products[i].measurement+'</div>'+
                        '<div class="mb-1 row col-12 dark">₹'+products[i].currentPrice+'</div>';

              if(products[i].subscriptionAvailability){
                elem=elem+'<div class="mb-1 row col-12 product-item-btns-holder">'+
                            '<button type="button" class="btn btn-success btn-subscribe">Subscribe @ ₹'+products[i].subscriptionPrice+'</button>'+
                          '</div>';
              }

              if(products[i].availableQuantity > 0){
                elem=elem+'<div class="row col-12 product-item-btns-holder">'+
                            '<button type="button" class="btn btn-light btn-buy-once">Buy Once</button>'+
                          '</div>'+
                          '<div class="col-12 text-center buy-once-prod-quantity-control just-border">'+
                            '<i class="fas fa-minus-circle ml-2 p-2"></i>'+
                            '<span class="count">1</span>'+
                            '<i class="fas fa-plus-circle  ml-2 p-2"></i>'+
                          '</div>';
              }
              else{
                elem=elem+'<div class="mb-1 row col-12">'+
                            '<button type="button" class="btn w-100 out-of-stock">Out of Stock</button>'+
                          '</div>';
              }
              elem=elem+'</div>';
             $('#all-deals').append(elem);
          }
          $('.buy-once-prod-quantity-control').hide();
        },
        error: function(resultData) {
          $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
             $("#ajax-error-alert").slideUp(500);
           });
         }
      });
    },

    storeMobile:function(mobile){
        localStorage.setItem("mobile", mobile);
        return true;
    },

    removeMobile : function(){
      localStorage.removeItem("mobile");
    },

    logout:function(){
      $.ajax({
        type: 'POST',
        url: "http://localhost/foru/profile/logout",
        contentType:"application/json",
        data: JSON.stringify({mobile: localStorage.getItem('mobile')}),
        success: function(resultData) {
          if(resultData){
            $('#user-profile').modal('hide');
            $('#user-profile-otp').modal('show');
            localStorage.removeItem('mobile');
            location.reload();
          }
        },
        error: function(resultData) {
          $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
             $("#ajax-error-alert").slideUp(500);
           });
         }
      });
    },

    logedinManagement:function(){
      $.ajax({
        type: 'POST',
        url: "http://localhost/foru/profile/isVerified",
        contentType:"application/json",
        data: JSON.stringify({mobile: localStorage.getItem('mobile')}),
        success : function(result){
          if(result){
            $('.home-info').show();
            $('.login-user-menu').show();
            $('.guest-user-menu').hide();
            $('.modal-content-full-width-logedin').show();
            $('.modal-content-full-width-guest').hide();
            $('.otp-sent-mobile').text(localStorage.getItem('mobile'));
            $('#my-profile-mobile').val(localStorage.getItem('mobile'));
          }else{
            $('.home-info').hide();
            $('.login-user-menu').hide();
            $('.guest-user-menu').show();
            $('.modal-content-full-width-logedin').hide();
            $('.modal-content-full-width-guest').show();
          }
        },
        error:function(){
          $('.home-info').hide();
          $('.login-user-menu').hide();
          $('.guest-user-menu').show();
          $('.modal-content-full-width-logedin').hide();
          $('.modal-content-full-width-guest').show();
          $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
             $("#ajax-error-alert").slideUp(500);
           });
        }
      });
      service.createCart();
      service.homeCartManagment();
    },

    homeCartManagment : function(){
      if(service.isCartAvaialable() && service.getCart().items.length >= 1){
          $('#home').addClass('home-bottom');
          $('#search .search-content').addClass('home-bottom');
          $('.home-cart-footer').show();
          let cart=service.getCart();
          $('.home-cart-footer .cart-total').text('₹'+cart.subtotal);
          $('.home-cart-footer .cart-items-count').text(cart.count + 'items');
      }
      else{
        $('#home').removeClass('home-bottom');
        $('#search .search-content').removeClass('home-bottom');
        $('.home-cart-footer').hide();
      }
    },

    productListCartManagement : function(){
      if(service.isCartAvaialable() && service.getCart().items.length >= 1){
        $('.product-content').addClass('product-content-bottom');
        $('.product-list-cart-footer').show();
        let cart=service.getCart();
        $('.product-list-cart-footer .cart-total').text('₹'+cart.subtotal);
        $('.product-list-cart-footer .cart-items-count').text(cart.count + 'items');
      }
      else{
        $('.product-content').removeClass('product-content-bottom');
        $('.product-list-cart-footer').hide();
      }
    },

    createCart : function(){
      if(!service.isCartAvaialable()){
        let cart=JSON.stringify({count : 0, items : new Array(), subtotal : 0, delivery : 0});
        localStorage.setItem("cart", cart);
      }
    },

    isCartAvaialable : function(){
      if(localStorage.getItem("cart"))
        return true;
    },

    getCart : function(){
      let cart=JSON.parse(localStorage.getItem("cart"));
      return cart;
    },

    emptyCart : function(){
      localStorage.removeItem("cart");
    },

    addTocart : function(skuId,image, brand, name, measure, price){
      let cart=service.getCart();
      let isExisting=false;
      for(i in cart.items){
        if(cart.items[i].skuId == skuId){
          cart.items[i].quantity=cart.items[i].quantity+1;
          isExisting=true;
          break;
        }
      }
      if(!isExisting){
        let product={
          skuId :skuId,
          image :image,
          brand :brand,
          name: name,
          measure:measure,
          price:price,
          quantity :1
        };
        cart.items.push(product);
      }

      cart.count=cart.count+1;
      cart.subtotal=cart.subtotal+Number(price);
      localStorage.setItem("cart",JSON.stringify(cart));
      service.homeCartManagment();
      service.productListCartManagement();
    },

    removeFromCart : function(skuId,price){
      let cart=service.getCart();
      let cartItems=cart.items;
      for(i in cartItems){
        if(cartItems[i].skuId == skuId && (cartItems[i].quantity-1)==0){
            cartItems.splice(i, 1);
        }
        else if(cartItems[i].skuId == skuId && (cartItems[i].quantity-1)>=1){
          cartItems[i].quantity=cartItems[i].quantity-1;
        }
      }
      cart.items=cartItems;
      cart.count=cart.count-1;
      cart.subtotal=cart.subtotal-Number(price);
      localStorage.setItem("cart",JSON.stringify(cart));
      service.homeCartManagment();
      service.productListCartManagement();
    },

    buildSubscritptionBag:function(){
      let subscriptionBag={items:[],frequency:'',startDate:'',serviceCharge:'',mobile:''};
      localStorage.setItem("subscriptionBag",JSON.stringify(subscriptionBag));
    },

    addSubscritionItem: function(brand,image,measure,name,price,quantity,skuId){
      let subscriptionBag=JSON.parse(localStorage.getItem("subscriptionBag"));
      subscriptionBag.items.push({brand:brand,image:image,measure:measure,name:name,price:price,quantity:quantity,skuId:skuId})
      localStorage.setItem("subscriptionBag",JSON.stringify(subscriptionBag));
    },

    updateSubscriptionItem: function(skuId,count){
      let subscriptionBag=JSON.parse(localStorage.getItem("subscriptionBag"));
      for(i in subscriptionBag.items){
        if(subscriptionBag.items[i].skuId==skuId){
          subscriptionBag.items[i].quantity=count;
          subscriptionBag.items[i].price=subscriptionBag.items[i].price*count;
        }
      }
      localStorage.setItem("subscriptionBag",JSON.stringify(subscriptionBag));
    },

    addSubscritionInfo: function(startDate,serviceCharge,frequency,mobile,preferredDays){
      let subscriptionBag=JSON.parse(localStorage.getItem("subscriptionBag"));
      subscriptionBag.startDate=startDate;
      subscriptionBag.serviceCharge=serviceCharge;
      subscriptionBag.frequency=frequency;
      subscriptionBag.mobile=mobile;
      subscriptionBag.preferredDays=preferredDays;
      localStorage.setItem("subscriptionBag",JSON.stringify(subscriptionBag));
    },

    getSubscriptionBag:function(){
      return JSON.parse(localStorage.getItem("subscriptionBag"));
    },

    buidMyOrders : function(){
      $.ajax({
        type: 'POST',
        url: "http://localhost/foru/order/mine",
        contentType:"application/json",
        data: JSON.stringify({mobile: localStorage.getItem('mobile'),date:$('#orders-content-date').find('.selected').attr('data-date-val')}),
        success : function(result){
          $('.orders-content').empty();
          if(result.orders.length==0)
            $('.orders-content').html('<div class="col-12 p-5 text-center"> No orders placed on the day</div>');
          for(i in result.orders){
            let elem='';
            switch(result.orders[i].orderStatus){
              case 'To be delivered':
                elem='<div class="row m-0 order p-2 mb-3" data-order-id="'+result.orders[i].order.id+'">'+
                        '<div class="row m-0 col-12 pb-3 hr">'+
                          '<div class="col-6 p-0">'+
                              '<b>'+result.orders[i].orderTitle+'</b>'+
                            '</div>'+
                            '<div class="col-6 text-right p-0">'+
                              '<i class="fas fa-clock light to-be-delivered"></i>'+
                              'To be delivered'+
                            '</div>'+
                          '</div>';
                        for(j in result.orders[i].order.orderLines){
                          let skuId=result.orders[i].order.orderLines[j].productId;
                          elem=elem+'<div class="row m-0 col-12 pb-3 mt-3 hr">'+
                            '<div class="col-4 p-0">'+
                              '<div class="col-12 p-0 p-2 mb-2">'+
                                '<img class="mx-auto d-block img-thumbnail" src="'+result.orders[i].products[skuId].imageLocation+'">'+
                              '</div>'+
                              '<div class="col-12 p-0 source">'+
                                result.orders[i].order.orderSource+
                              '</div>'+
                            '</div>'+
                            '<div class="col-8 p-0">'+
                              '<div class="col-12 light">'+
                                result.orders[i].products[skuId].brand+
                              '</div>'+
                              '<div class="col-12 text-nowrap">'+
                                '<b>'+result.orders[i].products[skuId].name+'b>'+
                              '</div>'+
                              '<div class="col-12 light">'+
                                result.orders[i].products[skuId].measurement+
                              '</div>'+
                              '<div class="col-12 light">'+
                                'Qty : '+result.orders[i].order.orderLines[j].quantity+
                              '</div>'+
                              '<div class="row m-0 p-0 col-12">'+
                                '<div class="col-6 text-left">'+
                                  '<b>₹'+result.orders[i].order.orderLines[j].soldPrice+'</b>'+
                                '</div>';

                                if(j==0)
                                  elem=elem+'<div class="col-6 p-0 text-right">'+
                                    '<button type="button" class="btn cancel-order p-0">'+
                                      '<i class="far fa-trash-alt"></i>'+
                                    '</button>'+
                                  '</div>';

                              elem=elem+'</div>'+
                            '</div>'+
                          '</div>';
                        }

                        elem=elem+'<div class="row m-0 col-12 pb-1 p-0">'+
                          '<div class="col-1 p-0 justify-content-center align-self-center light">'+
                            '<i class="fas fa-info-circle"></i>'+
                          '</div>'+
                          '<div class="col-11 p-0 light">'+
                            '<small>'+
                              'Service Charges are per delivery and is charged at the time of packing the order. All orders to be delivered on a gievn day are consolidated into a single delivery at the time of packing.'+
                            '</small>'+
                          '</div>'+
                        '</div>'+
                      '</div>';
                break;
              case 'Delivered':
                elem='<div class="row m-0 order p-2 mb-3" data-order-id="'+result.orders[i].order.id+'">'+
                  '<div class="row m-0 col-12 pb-3 hr">'+
                    '<div class="col-6 p-0">'+
                      '<b>'+result.orders[i].orderTitle+'</b>'+
                    '</div>'+
                    '<div class="col-6 text-right p-0">'+
                      '<i class="fas fa-check-circle delivered"></i>'+
                      'Delivered'+
                    '</div>'+
                  '</div>';
                  for(j in result.orders[i].order.orderLines){
                    let skuId=result.orders[i].order.orderLines[j].productId;
                      elem=elem+'<div class="row m-0 col-12 pb-3 mt-3 hr">'+
                        '<div class="col-4 p-0">'+
                          '<div class="col-12 p-0 p-2 mb-2">'+
                            '<img class="mx-auto d-block img-thumbnail" src="'+result.orders[i].products[skuId].imageLocation+'">'+
                          '</div>'+
                          '<div class="col-12 p-0 source">'+
                            result.orders[i].order.orderSource+
                          '</div>'+
                        '</div>'+
                        '<div class="col-8 p-0">'+
                          '<div class="col-12 light">'+
                            result.orders[i].products[skuId].brand+
                          '</div>'+
                          '<div class="col-12 text-nowrap">'+
                            '<b>'+result.orders[i].products[skuId].name+'</b>'+
                          '</div>'+
                          '<div class="col-12 light">'+
                            result.orders[i].products[skuId].measurement+
                          '</div>'+
                          '<div class="col-12 light">'+
                            'Qty : '+result.orders[i].order.orderLines[j].quantity+
                          '</div>'+
                          '<div class="row m-0 p-0 col-12">'+
                            '<div class="col-6 text-left">'+
                              '<b>₹'+result.orders[i].order.orderLines[j].soldPrice+'</b>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>';
                  }

                  elem=elem+'<div class="row m-0 col-12 p-2 mb-2 hr">'+
                    '<div class="col-6 mb-1 p-0 light">'+
                      'Sub Total :'+
                    '</div>'+
                    '<div class="col-6 p-0 text-right">'+
                      '<b>₹'+result.orders[i].order.subTotal+'</b>'+
                    '</div>'+
                    '<div class="col-6 mb-1 p-0 light">'+
                      'Service Charge :'+
                    '</div>'+
                    '<div class="col-6 p-0 text-right">'+
                      '<b>₹'+result.orders[i].order.serviceCharge+'</b>'+
                    '</div>'+
                    '<div class="col-6 mb-1 p-0 delivered">'+
                      'Savings :'+
                    '</div>'+
                    '<div class="col-6 p-0 delivered text-right">'+
                      '<b>₹'+result.orders[i].order.savings+'</b>'+
                    '</div>'+
                  '</div>'+
                  '<div class="row m-0 col-12 p-2 mb-2">'+
                    '<div class="col-6 p-0">'+
                        'Total :'+
                    '</div>'+
                    '<div class="col-6 p-0 text-right">'+
                      '<b>₹'+result.orders[i].order.total+'</b>'+
                    '</div>'+
                  '</div>'+
                '</div>';
                break;
              case 'Refunded':
              case 'Cancelled':
              case 'Failed':
                elem='<div class="row m-0 order p-2 mb-3" data-order-id="'+result.orders[i].order.id+'">'+
                        '<div class="row m-0 col-12 pb-3 hr">'+
                          '<div class="col-6 p-0">'+
                            '<b>'+result.orders[i].orderTitle+'</b>'+
                          '</div>'+
                          '<div class="col-6 text-right p-0">';
                  if(result.orders[i].orderStatus=='Refunded')
                            elem=elem+'<i class="fas fa-redo refunded"></i>'+
                            'Refunded';
                  else if(result.orders[i].orderStatus=='Cancelled')
                            elem=elem+'<i class="fas fa-clock cancelled"></i>'+
                            'Cancelled';
                  else if(result.orders[i].orderStatus=='Failed')
                            elem=elem+'<i class="fas fa-clock cancelled"></i>'+
                            'Failed';
                  elem=elem+'</div>'+
                        '</div>';
                  for(j in result.orders[i].order.orderLines){
                      let skuId=result.orders[i].order.orderLines[j].productId;
                      elem=elem+'<div class="row m-0 col-12 pb-3 mt-3">'+
                        '<div class="col-4 p-0">'+
                          '<div class="col-12 p-0 p-2 mb-2">'+
                            '<img class="mx-auto d-block img-thumbnail" src="'+result.orders[i].products[skuId].imageLocation+'">'+
                          '</div>'+
                          '<div class="col-12 p-0 source">'+
                            result.orders[i].order.orderSource+
                          '</div>'+
                        '</div>'+
                        '<div class="col-8 p-0">'+
                          '<div class="col-12 light">'+
                            result.orders[i].products[skuId].brand+
                          '</div>'+
                          '<div class="col-12 text-nowrap">'+
                            '<b>'+result.orders[i].products[skuId].name+'b>'+
                          '</div>'+
                          '<div class="col-12 light">'+
                            result.orders[i].products[skuId].measurement+
                          '</div>'+
                          '<div class="col-12 light">'+
                            'Qty : '+result.orders[i].order.orderLines[j].quantity+
                          '</div>'+
                          '<div class="row m-0 p-0 col-12">'+
                            '<div class="col-6 text-left">'+
                              '₹0 <s>₹'+result.orders[i].order.orderLines[j].soldPrice+'</s>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>';
                  }
                  elem=elem+'</div>';
                break;
            }
            $('.orders-content').append(elem);
          }
        },
        error:function(){
          $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
             $("#ajax-error-alert").slideUp(500);
           });
        }
      });
    },

    payAccount : function(amount,transactionType){
      $.ajax({
              type: 'POST',
              url: "http://localhost/foru/account/pay",
              contentType:"application/json",
              data: JSON.stringify({mobile: localStorage.getItem('mobile'),amount: amount,transactionType:transactionType}),
              success : function(result){
                if(result){
                  $('#accounting-pay-money-dialog-back').trigger('click');
                  $('#myaccount-accounting-link').trigger('click');
                }else{
                  $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                     $("#ajax-error-alert").slideUp(500);
                   });
                }
              },
              error:function(){
                $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                   $("#ajax-error-alert").slideUp(500);
                 });
              }
            });
    }
  };

  $(document).on('click','.modal-backdrop',function(){
    $('#user-menu').modal('hide');
  });

  $('.btn-login').click(function(){
    $('#user-menu').modal('hide');
    $('#user-profile').modal('show');
  });

  $(document).on("click","#shop-by-catagory-link",function(){
    $.ajax({
      type: 'POST',
      url: "http://localhost/foru/product/categories",
      contentType:"application/json",
      success: function(resultData) {
        for(i in resultData){
          let category=resultData[i].id;
          let name=resultData[i].name;
          let image=resultData[i].imageLocation;
          $('.catagories-content').append('<div class="col-4 category-item flex-fill" data-category="'+category+'">'+
                                                              '<div class="col-12">'+
                                                                '<img class="mx-auto d-block img-thumbnail" src="'+image+'">'+
                                                              '</div>'+
                                                              '<div class="col-12 text-center p-0">'+name+'</div>'+
                                                          '</div>');
        }
        $('.catagories-content').append('<div class="filler"></div>');
        $('#user-menu').modal('hide');
        $('#shop-by-catagory').modal('show');
        setTimeout(function(){ makeFillRemainSpace('.catagories-content .filler');}, 300);
      },
      error: function(resultData) {
        $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
           $("#ajax-error-alert").slideUp(500);
         });
       }
    });
  });

  $('#shop-by-catagory-back').click(function(){
    $('#shop-by-catagory').modal('hide');
  });

  $(document).on("click",".category-item,#all-milk-view-all",function() {
    let category='Milk';
    if($(this).hasClass('category-item')){
      category=$(this).attr('data-category');
    }
    $.ajax({
      type: 'POST',
      url: "http://localhost/foru/product/category-products",
      contentType:"application/json",
      data :JSON.stringify({category : category}),
      success: function(resultData) {
        let products=resultData.products;
        let category=resultData.category;

        $('.product-subcatagory-header').empty();
        $('#product-list-back').next().text(category.name);
        for(i in category.subCategory){
          let elem='<a href="javascript:void(0)" class="inline-block product-sub-catagory">'+category.subCategory[i]+'</a>';
          $('.product-subcatagory-header').append(elem);
        }
        $('.product-subcatagory-header .product-sub-catagory').eq(0).addClass('active');

        $('#product-list .modal-dialog-full-width .modal-content .product-content').empty();
        for(i in products){
          let elem='<div class="col-6 product-item mr-auto" data-sku-id="'+products[i].id+'" data-sub-catagory="'+products[i].subCategory+'">'+
                      '<div class="col-12">'+
                        '<img class="mx-auto d-block img-thumbnail" src="'+products[i].imageLocation+'">'+
                      '</div>'+
                      '<div class="mb-1 row col-12 light">'+products[i].brand+'</div>'+
                      '<div class="mb-1 row col-12 dark">'+products[i].name+'</div>'+
                      '<div class="mb-1 row col-12 light">'+products[i].measurement+'</div>'+
                      '<div class="mb-1 row col-12 dark">₹'+products[i].currentPrice+'</div>';

          if(products[i].subscriptionAvailability){
            elem=elem+'<div class="mb-1 row col-12 product-item-btns-holder">'+
                        '<button type="button" class="btn btn-success btn-subscribe">Subscribe @ ₹'+products[i].subscriptionPrice+'</button>'+
                      '</div>';
          }

          if(products[i].availableQuantity > 0){
            elem=elem+'<div class="row col-12 product-item-btns-holder">'+
                        '<button type="button" class="btn btn-light btn-buy-once">Buy Once</button>'+
                      '</div>'+
                      '<div class="col-12 text-center buy-once-prod-quantity-control just-border" style="display: none;">'+
                        '<i class="fas fa-minus-circle ml-2 p-2"></i>'+
                        '<span class="count">1</span>'+
                        '<i class="fas fa-plus-circle  ml-2 p-2"></i>'+
                      '</div>';
          }
          else{
            elem=elem+'<div class="mb-1 row col-12">'+
                        '<button type="button" class="btn w-100 out-of-stock">Out of Stock</button>'+
                      '</div>';
          }
          elem=elem+'</div>';

          $('#product-list .modal-dialog-full-width .modal-content .product-content').append(elem);
        }

        $('#product-list .modal-dialog-full-width .modal-content .product-content').append('<div class="filler"></div>');

        setTimeout(function(){ makeFillRemainSpace('.product-content .filler');}, 300);

        $('#shop-by-catagory').modal('hide');
        $('#product-list').modal('show');
        $('.buy-once-prod-quantity-control').hide();
      },
      error: function(resultData) {
        $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
           $("#ajax-error-alert").slideUp(500);
         });
       }
    });
    service.productListCartManagement();
    });

  $('#need-help-link').click(function(){
    $('#user-menu').modal('hide');
    $('.index-footer .footer-menu').eq(3).trigger('click');
  });

  $('#product-list-back').click(function(){
    $('#product-list').modal('hide');
  });

  $(document).on("click",".product-sub-catagory",function(){
    $('.product-sub-catagory').removeClass('active');
    $(this).addClass('active');
    let subcatagory=$(this).text().trim();
    $('#product-list .product-content .product-item').each(function(){
      let thisSubCategory=$(this).attr('data-sub-catagory').split(',');
      if(thisSubCategory.includes(subcatagory)){
        $(this).show();
      }else{
        $(this).hide();
      }
    });
  });

  $(document).on("click","#home-promotions-1 img, #home-promotions-2 img, .home-bottom-promotion img",function(){
    let carouselItem=$(this).parent();
    let isInfo=carouselItem.attr('data-is-info');
    let isCategory=carouselItem.attr('data-is-category');
    let isProduct=carouselItem.attr('data-is-product');
    let category=carouselItem.attr('data-category');
    //let product=carouselItem.attr('data-product');
    if(isCategory)
      $.ajax({
        type: 'POST',
        url: "http://localhost/foru/product/category-products",
        contentType:"application/json",
        data :JSON.stringify({category : category}),
        success: function(resultData) {
          let products=resultData.products;
          let category=resultData.category;

          $('.product-subcatagory-header').empty();
          $('#product-list-back').next().text(category.name);
          for(i in category.subCategory){
            let elem='<a href="javascript:void(0)" class="inline-block product-sub-catagory">'+category.subCategory[i]+'</a>';
            $('.product-subcatagory-header').append(elem);
          }
          $('.product-subcatagory-header .product-sub-catagory').eq(0).addClass('active');

          $('#product-list .modal-dialog-full-width .modal-content .product-content').empty();
          for(i in products){
            let elem='<div class="col-6 product-item mr-auto" data-sku-id="'+products[i].id+'" data-sub-catagory="'+products[i].subCategory+'">'+
                        '<div class="col-12">'+
                          '<img class="mx-auto d-block img-thumbnail" src="'+products[i].imageLocation+'">'+
                        '</div>'+
                        '<div class="mb-1 row col-12 light">'+products[i].brand+'</div>'+
                        '<div class="mb-1 row col-12 dark">'+products[i].name+'</div>'+
                        '<div class="mb-1 row col-12 light">'+products[i].measurement+'</div>'+
                        '<div class="mb-1 row col-12 dark">₹'+products[i].currentPrice+'</div>';

            if(products[i].subscriptionAvailability){
              elem=elem+'<div class="mb-1 row col-12 product-item-btns-holder">'+
                          '<button type="button" class="btn btn-success btn-subscribe">Subscribe @ ₹'+products[i].subscriptionPrice+'</button>'+
                        '</div>';
            }

            if(products[i].availableQuantity > 0){
              elem=elem+'<div class="row col-12 product-item-btns-holder">'+
                          '<button type="button" class="btn btn-light btn-buy-once">Buy Once</button>'+
                        '</div>'+
                        '<div class="col-12 text-center buy-once-prod-quantity-control just-border" style="display: none;">'+
                          '<i class="fas fa-minus-circle ml-2 p-2"></i>'+
                          '<span class="count">1</span>'+
                          '<i class="fas fa-plus-circle  ml-2 p-2"></i>'+
                        '</div>';
            }
            else{
              elem=elem+'<div class="mb-1 row col-12">'+
                          '<button type="button" class="btn w-100 out-of-stock">Out of Stock</button>'+
                        '</div>';
            }
            elem=elem+'</div>';

            $('#product-list .modal-dialog-full-width .modal-content .product-content').append(elem);
          }

          $('#product-list .modal-dialog-full-width .modal-content .product-content').append('<div class="filler"></div>');

          setTimeout(function(){ makeFillRemainSpace('.product-content .filler');}, 500);

          $('#product-list').modal('show');
          $('.buy-once-prod-quantity-control').hide();
        },
        error: function(resultData) {
          $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
             $("#ajax-error-alert").slideUp(500);
           });
         }
      });

    service.productListCartManagement();
  });

  $('#ajax-error-alert').hide();

  $('#login-to-otp').click(function(){
    $("#login-mobile").attr('style','');
    let phone=$("#login-mobile").val();
    $('.otp-sent-mobile').text(phone);
    $('#my-profile-mobile').val(phone);
    if(phone.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)){
      $.ajax({
        type: 'POST',
        url: "http://localhost/foru/profile/new",
        contentType:"application/json",
        data: JSON.stringify({mobile: phone}),
        success: function(resultData) {
          if(resultData){
            $('#user-profile').modal('hide');
            $('#user-profile-otp').modal('show');
          }
          else {
            $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
               $("#ajax-error-alert").slideUp(500);
             });
          }
        },
        error: function(resultData) {
          $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
             $("#ajax-error-alert").slideUp(500);
           });
         }
      });
    }
    else{
      $("#login-mobile").attr('style','border-color: #dc3545;');
      $("#login-mobile").focus();
    }

  });

  $('#check-otp-alert').hide();
  $('#login-after-otp').click(function(){
    let phone=$("#login-mobile").val();
    let otp=$('#login-mobile-OTP').val();
    if(otp!=''){
      service.storeMobile(phone);
      $.ajax({
        type: 'POST',
        url: "http://localhost/foru/profile/verify",
        contentType:"application/json",
        data: JSON.stringify({mobile: phone,'otp':otp}),
        success: function(resultData) {
          if(resultData){
            $('#user-profile-otp').modal('hide');
            $('#login-mobile-OTP').val('');
            service.logedinManagement();
          }
          else {
            $("#check-otp-alert").fadeTo(2000, 500).slideUp(500, function() {
               $("#check-otp-alert").slideUp(500);
             });
              service.removeMobile();
          }
        },
        error: function(resultData) {
          $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
             $("#ajax-error-alert").slideUp(500);
           });
           service.removeMobile();
         }
      });
    }
  });

  $('#mobile-otp-change').click(function(){
    $('#user-profile').modal('show');
    $('#user-profile-otp').modal('hide');
  });

  $('#user-profile-back').click(function(){
    $('#user-profile').modal('hide');
  });

  //subscriptions operations
  service.buildSubscritptionBag();
  $(document).on('click', '.btn-subscribe', function(){
  let productItem=$(this).parent().parent();
    $.ajax({
      type: 'POST',
      url: "http://localhost/foru/profile/isVerified",
      contentType:"application/json",
      data: JSON.stringify({mobile: localStorage.getItem('mobile')}),
      success : function(result){
        if(result){
          let skuId=productItem.attr('data-sku-id');
          let image=productItem.find('img').attr('src');
          let brand=productItem.find('div').eq(1).text();
          let name=productItem.find('div').eq(2).text();
          let measure=productItem.find('div').eq(3).text();
          let price=productItem.find('div').eq(4).text().split('₹')[1];

          $('.subscription-cart-content .products').empty();
          let elem='<div class="row m-0 col-12 p-2" data-sku-id="'+skuId+'">'+
                      '<div class="col-2 text-left p-0">'+
                        '<img class="mx-auto d-block img-thumbnail" src="'+image+'">'+
                      '</div>'+
                      '<div class="col-6">'+
                        '<div class="col-12">'+brand+'</div>'+
                        '<div class="col-12 text-nowrap"><h6>'+name+'</h6></div>'+
                        '<div class="col-12">'+measure+' </div>'+
                        '<div class="col-12 text-nowrap">Subscription Price : <span class="price" data-product-price="'+price+'">₹'+price+'</span></div>'+
                        '<div class="col-12 text-nowrap">*Price may change as per market charges</div>'+
                      '</div>'+
                      '<div class="col-4 p-0 text-left prod-quantity-control">'+
                        '<i class="fas fa-minus-circle"></i>'+
                        '<span class="count">1</span>'+
                        '<i class="fas fa-plus-circle"></i>'+
                      '</div>'+
                    '</div>';

          $('.subscription-cart-content .products').append(elem);
          $('#subscription-cart').modal('show');
          let count=parseInt($('.subscription-cart-content .prod-quantity-control .count').text());
          service.addSubscritionItem(brand,image,measure,name,price,count,skuId);
          if(count<=1)
            $('.prod-quantity-control i').eq(0).addClass('inactive');
        }else{
          $('#user-profile').modal('show');
        }
      },
      error:function(){
        $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
           $("#ajax-error-alert").slideUp(500);
         });
      }
    });
  });

  $(document).on('click','.subscription-cart-content .prod-quantity-control i',function(){
    let isMinus=$(this).hasClass('fa-minus-circle');
    let isActive=(!$(this).hasClass('inactive'));
    let count=parseInt($('.prod-quantity-control .count').text());
    let skuId=$(this).parent().parent().attr('data-sku-id');
    if(isMinus && isActive){
      $('.prod-quantity-control .count').text(count-1);
      service.updateSubscriptionItem(skuId,count-1);
      if(count-1 == 1)
        $(this).addClass('inactive');
    }
    else if(isActive){
      $('.prod-quantity-control .count').text(count+1);
      $('.prod-quantity-control i').removeClass('inactive');
      service.updateSubscriptionItem(skuId,count+1);
    }
  });

  $('.subscription-cart-content .prod-quantity-control .count').on('DOMSubtreeModified',function(){
    let count=parseInt($('.prod-quantity-control .count').text());
    let priceEach=parseFloat($('.subscription-cart-content .price').attr('data-product-price'));
    let totalPrice=count*priceEach;
    $('.subscription-cart-content .price').text(totalPrice);
  });

  $('#subscription-cart-back').click(function(){
    $('#subscription-cart').modal('hide');
    $('#subscription-date-block').hide();
    $('.subscription-cart-footer').hide();
    $('#subscription-daily-schedule-link,#subscription-alternateday-schedule-link,#subscription-threedays-schedule-link').removeClass('btn-success');
    $('.subscription-cart-content').removeClass('subscription-cart-content-bottom');
  });

  $('#subscription-date-block').hide();
  $('.subscription-cart-footer').hide();
  $('#subscription-daily-schedule-link,#subscription-alternateday-schedule-link,#subscription-threedays-schedule-link').bootstrapMaterialDatePicker({
     weekStart : 0,
     time: false,
     minDate : new Date(),
     maxDate : new Date(new Date().setMonth(new Date().getMonth()+1))
   }).on('change',function(e, date){
     $('#subscription-daily-schedule-link,#subscription-alternateday-schedule-link,#subscription-threedays-schedule-link,#subscription-weekly-schedule-link,#subscription-monthly-schedule-link').removeClass('btn-success');
     let elemId=e.currentTarget.id;
     let dateInfo=date._d.toString().split(' ');
     let startDate=dateInfo[0]+' , '+dateInfo[1]+' '+dateInfo[2]+' '+dateInfo[3];
     $('#'+elemId).addClass('btn-success');
     $('.subscription-cart-content').addClass('subscription-cart-content-bottom');
     $('#subscription-date-block').show();
     $('#subscription-date-block .start-date').text(startDate);

     $('.subscription-cart-footer').show();
     $('.subscription-cart-footer .start-date').text(startDate);
   });

   $('#subscription-start-date').bootstrapMaterialDatePicker({
      weekStart : 0,
      time: false,
      minDate : new Date(),
      maxDate : new Date(new Date().setMonth(new Date().getMonth()+1))
    }).on('change',function(e, date){
      $('.subscription-cart-content').addClass('subscription-cart-content-bottom');
      $('#subscription-date-block').show();
      let dateInfo=date._d.toString().split(' ');
      let startDate=dateInfo[0]+' , '+dateInfo[1]+' '+dateInfo[2]+' '+dateInfo[3];
      $('#subscription-date-block .start-date').text(startDate);

      $('.subscription-cart-footer').show();
      $('.subscription-cart-footer .start-date').text(startDate);
    });

    $('#subscription-weekly-schedule-link').click(function(){
      $('#subscription-weekly-schedule-link-modal').modal('show');
      $('#subscription-date-block').hide();
      $('.subscription-cart-footer').hide();
    });

    $('#subscription-weekly-start-date-submit').click(function(){
      $('#subscription-weekly-schedule-link-modal').modal('hide');
    });

    $('#subscription-weekly-start-date').bootstrapMaterialDatePicker({
       weekStart : 0,
       time: false,
       minDate : new Date(),
       maxDate : new Date(new Date().setMonth(new Date().getMonth()+1))
     }).on('change',function(e, date){
       $('.subscription-cart-content').addClass('subscription-cart-content-bottom');
       $('#subscription-date-block').show();
       let dateInfo=date._d.toString().split(' ');
       let startDate=dateInfo[0]+' , '+dateInfo[1]+' '+dateInfo[2]+' '+dateInfo[3];
       $('#subscription-date-block .start-date').text(startDate);

       $('.subscription-cart-footer').show();
       $('.subscription-cart-footer .start-date').text(startDate);
       $('.weekly-subscription-start-date .start-date').text(startDate);
       $('#subscription-daily-schedule-link,#subscription-alternateday-schedule-link,#subscription-threedays-schedule-link,#subscription-weekly-schedule-link,#subscription-monthly-schedule-link').removeClass('btn-success');
       $('#subscription-weekly-schedule-link').addClass('btn-success');
     });

     $('#subscription-monthly-schedule-link').click(function(){
       $('#subscription-monthly-schedule-link-modal').modal('show');
       $('#subscription-date-block').hide();
       $('.subscription-cart-footer').hide();
     });

     $('#subscription-monthly-schedule-link-modal table tbody tr td').click(function(){
       let isSelectedAlready=$(this).hasClass('selected');
       if(isSelectedAlready)
        $(this).removeClass('selected');
      else
        $(this).addClass('selected');
     });

     $('#subscription-monthly-start-date-submit').click(function(){
       $('#subscription-monthly-schedule-link-modal').modal('hide');
     });

     $('#subscription-monthly-start-date').bootstrapMaterialDatePicker({
        weekStart : 0,
        time: false,
        minDate : new Date(),
        maxDate : new Date(new Date().setMonth(new Date().getMonth()+1))
      }).on('change',function(e, date){
        $('.subscription-cart-content').addClass('subscription-cart-content-bottom');
        $('#subscription-date-block').show();
        let dateInfo=date._d.toString().split(' ');
        let startDate=dateInfo[0]+' , '+dateInfo[1]+' '+dateInfo[2]+' '+dateInfo[3];
        $('#subscription-date-block .start-date').text(startDate);

        $('.subscription-cart-footer').show();
        $('.subscription-cart-footer .start-date').text(startDate);
        $('.montly-subscription-start-date .start-date').text(startDate);
        $('#subscription-daily-schedule-link,#subscription-alternateday-schedule-link,#subscription-threedays-schedule-link,#subscription-weekly-schedule-link,#subscription-monthly-schedule-link').removeClass('btn-success');
        $('#subscription-monthly-schedule-link').addClass('btn-success');
      });

      $('#subscription-confirmation').click(function(){
        let startDate=$('#subscription-date-block').prev().find('.btn-success').attr('value');
        let serviceCharge=$('#subscription-date-block').find('div').eq(5).text().split('₹')[1];
        let frequency=$('#subscription-date-block').prev().find('.btn-success').text().toUpperCase().replaceAll(" ", "_");;
        let mobile=localStorage.getItem('mobile');
        let preferredDays=[];
        switch (frequency) {
          case "WEEKLY":
              $('#subscription-weekly-schedule-link-modal .modal-dialog .modal-content .modal-body .container .form-check').each(function(){
                if ($(this).find('.form-check-input').prop('checked')==true){
                      preferredDays.push($(this).find('.form-check-label').text().trim());
                  }
              });
              startDate=$('#subscription-weekly-start-date').attr('value');
            break;
            case "MONTHLY":
                $('#subscription-monthly-schedule-link-modal .modal-dialog .modal-content .modal-body .container table tbody tr td.selected').each(function(){
                  preferredDays.push($(this).text().trim());
                });
                startDate=$('#subscription-monthly-start-date').attr('value');
              break;
        }
        service.addSubscritionInfo(startDate,serviceCharge,frequency,mobile,preferredDays);
        $.ajax({
          type: 'POST',
          url: "http://localhost/foru/subscriptions/new",
          contentType:"application/json",
          data: JSON.stringify({subscriptionBag: JSON.stringify(service.getSubscriptionBag())}),
          success : function(result){
            if(result.id){
              let startDate = moment(service.getSubscriptionBag().startDate, 'YYYY-MM-DD')
              $('.subscription-cart-content-success .section').eq(0).find('div').eq(3).find('div').find('b').text(startDate.format('MMM DD,YYYY'));
              $('#subscription-succesful').modal('show');
              $('#subscription-cart').modal('hide');
            }
            else {
              $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                 $("#ajax-error-alert").slideUp(500);
               });
            }
            service.buildSubscritptionBag();
          },
          error:function(){
            $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
               $("#ajax-error-alert").slideUp(500);
             });
          }
        });

      });

      $('#subscription-cart-success-back').click(function(){
          $('#subscription-succesful').modal('hide');
      });


      //buy once operations.
      $('.buy-once-prod-quantity-control').hide();
      $(document).on('click', '.btn-buy-once', function(){
        let productItem=$(this).parent().parent();
        $.ajax({
          type: 'POST',
          url: "http://localhost/foru/profile/isVerified",
          contentType:"application/json",
          data: JSON.stringify({mobile: localStorage.getItem('mobile')}),
          success : function(result){
            if(result){
              let skuId=productItem.attr('data-sku-id');
              let image=productItem.find('img').attr('src');
              let brand=productItem.find('div').eq(1).text();
              let name=productItem.find('div').eq(2).text();
              let measure=productItem.find('div').eq(3).text();
              let price=productItem.find('div').eq(4).text().split('₹')[1];
              service.addTocart(skuId,image,brand,name,measure,price);
              productItem.find('.buy-once-prod-quantity-control').show();
              productItem.find('.btn-buy-once').hide();
            }else{
              $('#user-profile').modal('show');
            }
          },
          error:function(){
            $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
               $("#ajax-error-alert").slideUp(500);
             });
          }
        });
      });

      $(document).on('click', '.buy-once-prod-quantity-control i', function(){
        let count=parseInt($(this).parent().find('.count').text());
        let productItem=$(this).parent().parent();
        let skuId=productItem.attr('data-sku-id');
        let image=productItem.find('img').attr('src');
        let brand=productItem.find('div').eq(1).text();
        let name=productItem.find('div').eq(2).text();
        let measure=productItem.find('div').eq(3).text();
        let price=productItem.find('div').eq(4).text().split('₹')[1];
        if($(this).hasClass('fa-minus-circle')){
          if(count==1){
            $(this).parent().hide();
            $(this).parent().parent().find('.btn-buy-once').show();
          }
          else {
            $(this).parent().find('.count').text(count-1);
          }
          service.removeFromCart(skuId,price);
        }
        else{
          $(this).parent().find('.count').text(count+1);
          service.addTocart(skuId,image,brand,name,measure,price);
        }
      });

      //cart opearations
      $('.view-cart').click(function(){
        $('#my-cart-page').modal('show');
        $('#cart-items').empty();
        let cart=service.getCart();
        for(i in cart.items){
          let itemsElem='<div class="row m-0 col-12 p-2" data-sku-id="'+cart.items[i].skuId+'">'+
                          '<div class="col-2 text-left p-0">'+
                            '<img class="mx-auto d-block img-thumbnail" src="'+cart.items[i].image+'">'+
                          '</div>'+
                          '<div class="col-6">'+
                            '<div class="col-12">'+cart.items[i].brand+'</div>'+
                            '<div class="col-12"><h6>'+cart.items[i].name+'</h6></div>'+
                            '<div class="col-12">'+cart.items[i].measure+' </div>'+
                            '<div class="col-12 text-nowrap">₹'+cart.items[i].price+'</div>'+
                            '<div class="col-12 text-nowrap">*Price may change as per market charges</div>'+
                          '</div>'+
                          '<div class="col-4 p-0 text-right my-cart-quantity-control">'+
                              '<i class="fas fa-minus-circle p-2"></i>'+
                              '<span class="count">'+cart.items[i].quantity+'</span>'+
                              '<i class="fas fa-plus-circle p-2"></i>'+
                            '</div>'+
                          '</div>';

            $('#cart-items').append(itemsElem);
            if(i < cart.items.length-1)
              $('#cart-items').append('<div class="mt-2 mb-2 col-12 hr"></div>');
        }

        $('.my-cart-footer .cart-total').text('₹'+cart.subtotal);
        $('.my-cart-footer .cart-items-count').text(cart.count+' items');
        $('#my-cart-page .cart-count').text(cart.count+' cart items');
      });

      $('#my-cart-back').click(function(){
        $('#my-cart-page').modal('hide');
      });

      $('#place-order').click(function(){
        let cart=JSON.stringify(service.getCart());
        let mobile=localStorage.getItem('mobile');
        let deliveryDate=$('#my-cart-delivery-date').attr('value');
        let isFinal=true;
        $.ajax({
          type: 'POST',
          url: "http://localhost/foru/order/new",
          contentType:"application/json",
          data: JSON.stringify({cart:cart,mobile:mobile,deliveryAt:deliveryDate,isFinal:isFinal}),
          success : function(result){
            if(result!=="FAILED"){
              $('#my-cart-page').modal('hide');
              $('#cart-succesful').modal('show');
              service.emptyCart();
              service.homeCartManagment();
              service.productListCartManagement();
              service.createCart();
              $('.btn-buy-once').show();
              $('.buy-once-prod-quantity-control .count').text(1);
              $('.buy-once-prod-quantity-control').hide();
            }else{
              $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                 $("#ajax-error-alert").slideUp(500);
               });
            }
          },
          error:function(){
            $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
               $("#ajax-error-alert").slideUp(500);
             });
          }
        });
      });

      $('#cart-success-back').click(function(){
          $('#cart-succesful').modal('hide');
      });

      $('#my-cart-delivery-date').bootstrapMaterialDatePicker({
         weekStart : 0,
         time: false,
         minDate : new Date(),
         maxDate : new Date(new Date().setMonth(new Date().getMonth()+1))
       }).on('change',function(e, date){
         let dateInfo=date._d.toString().split(' ');
         let startDate=dateInfo[0]+' , '+dateInfo[1]+' '+dateInfo[2]+' '+dateInfo[3];
         $('.my-cart-content .my-cart-selected-delivery-date').text(startDate);
       });
       $('#my-cart-delivery-date').val(moment(new Date()).format("YYYY-MM-DD"));
       $('.my-cart-content .my-cart-selected-delivery-date').text(moment(new Date()).format("ddd, MMM D YYYY"));

       $(document).on('click', '.my-cart-quantity-control i', function(){
         let count=parseInt($(this).parent().find('.count').text());
         let productItem=$(this).parent().parent();
         let skuId=productItem.attr('data-sku-id');
         let image=productItem.find('img').attr('src');
         let brand=productItem.find('div').eq(1).find('div').eq(0).text();
         let name=productItem.find('div').eq(1).find('div').eq(1).text();
         let measure=productItem.find('div').eq(1).find('div').eq(2).text();
         let price=productItem.find('div').eq(1).find('div').eq(3).text().split('₹')[1];
         if($(this).hasClass('fa-minus-circle')){
           if(count-1 == 0){
             service.removeFromCart(skuId,price);
             productItem.prev('.hr').remove();
             productItem.remove();
           }
           else{
             $(this).parent().find('.count').text(count-1);
              service.removeFromCart(skuId,price);
           }
         }
         else{
           $(this).parent().find('.count').text(count+1);
           service.addTocart(skuId,image,brand,name,measure,price);
         }

         let cart=service.getCart();
         $('.my-cart-footer .cart-total').text('₹'+cart.subtotal);
         $('.my-cart-footer .cart-items-count').text(cart.count+' items');
         $('#my-cart-page .cart-count').text(cart.count+' cart items');
         if(cart.items.length==0){
           $('#my-cart-page').modal('hide');
           service.emptyCart();
           service.homeCartManagment();
           service.productListCartManagement();
           service.createCart();
           $('.btn-buy-once').show();
           $('.buy-once-prod-quantity-control .count').text(1);
           $('.buy-once-prod-quantity-control').hide();
         }

       });

       $('.continue-shopping').click(function(){
         $('#cart-succesful').modal('hide');
         $('#subscription-succesful').modal('hide');
       });

       //footer menu starts here
       $('#home').show();
       $('#orders,#accounting,#help,#search').hide();
       $('.index-footer .footer-menu').click(function(){
         let thisElem=$(this);
         let clickedIcon=thisElem.find('span').text();
         switch (clickedIcon) {
           case 'Home':
             $('#home').show();
             $('#orders,#accounting,#help,#search').hide();
             $('.index-footer .footer-menu').removeClass('footer-menu-active');
             thisElem.addClass('footer-menu-active');
             break;
           case 'Orders':
             $.ajax({
               type: 'POST',
               url: "http://localhost/foru/profile/isVerified",
               contentType:"application/json",
               data: JSON.stringify({mobile: localStorage.getItem('mobile')}),
               success : function(result){
                 if(result){
                   $('#orders').show();
                   $('#home,#accounting,#help,#search').hide();
                   setCurrentDate();
                   setTimeout(function(){ makeFillRemainSpace('.orders-content .filler');}, 300);
                   $('.index-footer .footer-menu').removeClass('footer-menu-active');
                   thisElem.addClass('footer-menu-active');
                   service.buidMyOrders();
                 }else{
                   $('#user-profile').modal('show');
                 }
               },
               error:function(){
                 $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                    $("#ajax-error-alert").slideUp(500);
                  });
               }
             });
             break;
           case 'Accounting':
             $.ajax({
               type: 'POST',
               url: "http://localhost/foru/profile/isVerified",
               contentType:"application/json",
               data: JSON.stringify({mobile: localStorage.getItem('mobile')}),
               success : function(result){
                 if(result){

                   $.ajax({
                        type: 'POST',
                        url: "http://localhost/foru/account/mine",
                        contentType:"application/json",
                        data: JSON.stringify({mobile: localStorage.getItem('mobile')}),
                        success : function(myResult){

                          $('.accounting-balance b').text('₹'+myResult.balance);
                          let cssClass=myResult.balance<0?'red':'green';

                          $('.my-accounting-summary').find('.row').eq(0).find('div').eq(1).text('₹'+myResult.myAccountingSummary.lastPaidAmount);
                          $('.my-accounting-summary').find('.row').eq(1).find('div').eq(1).text('₹'+myResult.myAccountingSummary.balanceAfterLastPayment);
                          $('.my-accounting-summary').find('.row').eq(2).find('div').eq(1).text('₹'+myResult.myAccountingSummary.billsSinceLastPayment);
                          $('.my-accounting-summary').find('.row').eq(3).find('div').eq(1).text('₹'+myResult.myAccountingSummary.monthlyAverageExpense);

                          $('.accounting-balance').addClass(cssClass);
                          $('#accounting').show();
                          $('#orders,#home,#help,#search').hide();
                          $('.index-footer .footer-menu').removeClass('footer-menu-active');
                          thisElem.addClass('footer-menu-active');
                        },
                        error:function(){
                          $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                             $("#ajax-error-alert").slideUp(500);
                           });
                        }
                      });

                 }else{
                   $('#user-profile').modal('show');
                 }
               },
               error:function(){
                 $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                    $("#ajax-error-alert").slideUp(500);
                  });
               }
             });
             break;
           case 'Help':
             $('#help').show();
             $('#orders,#accounting,#home,#search').hide();
             setTimeout(function(){ makeFillRemainSpace('.help-content .filler');}, 300);
             $('.index-footer .footer-menu').removeClass('footer-menu-active');
             thisElem.addClass('footer-menu-active');
             break;
         }
       });

       //my order section starts here.
       let datesThreeMonths=DateUtil.getDaysAroundThreeMonths();
       let datesThree=datesThreeMonths.dates;
       for(i in datesThree){
         let date=datesThree[i].dayWord+'-'+datesThree[i].dayNo+'-'+datesThree[i].month+'-'+datesThree[i].year;
         let dateElem='<div class="col-2 text-center date p-2" data-date-val="'+date+'">'+
                         '<div class="col-12 p-0">'+datesThree[i].dayWord+'</div>'+
                         '<div class="col-12 p-0">'+datesThree[i].dayNo+'</div>'+
                       '</div>';
        $('#orders-content-date').append(dateElem);
       }

       function setCurrentDate(){
         let today=datesThreeMonths.today;
         $('#orders-content-date .date').each(function(){
           let dateAttr=$(this).attr('data-date-val').split('-');
           $('#orders-content-date').scrollLeft(0);
           if(dateAttr[0]==today.dayWord &&  dateAttr[1]==today.dayNo && dateAttr[2]==today.month &&  dateAttr[3]==today.year){
             $('#orders-content-date .date').removeClass('selected');
             $(this).addClass('selected');
             let selectedPrevTwiceBefore=$("#orders-content-date .selected").prev().prev().prev();
              $('#orders-content-date').scrollLeft(selectedPrevTwiceBefore.offset().left);
              return false;
           }
         });
       }

       $('#orders-content-date').on('scroll', function() {
          $('#orders-content-date .date').each(function(){
              let left=$(this).offset().left;
              if(left > 0){
                $('#orders-content-date .date').removeClass('selected');
                let elem=$(this).next().next();
                elem.addClass('selected');
                let dateAttr=elem.attr('data-date-val').split('-');
                $('.orders-header .selected-date').text(dateAttr[2]+' '+dateAttr[1]);
                service.buidMyOrders();
                return false;
              }
            });
        });

    $('#selected-date-btn').bootstrapMaterialDatePicker({
       weekStart : 0,
       time: false,
       minDate : new Date(new Date().setMonth(new Date().getMonth()-3)),
       maxDate : new Date(new Date().setMonth(new Date().getMonth()+3))
     }).on('change',function(e, date){
       let dateInfo=date._d.toString().split(' ');
       $('#orders-content-date .date').each(function(){
         let dateAttr=$(this).attr('data-date-val').split('-');
         $('#orders-content-date').scrollLeft(0);
         if(dateAttr[0]==dateInfo[0].toUpperCase() &&  dateAttr[1]==dateInfo[2] && dateAttr[2]==dateInfo[1] &&  dateAttr[3]==dateInfo[3]){
           $('#orders-content-date .date').removeClass('selected');
           $(this).addClass('selected');
           $('.orders-header .selected-date').text(dateAttr[2]+' '+dateAttr[1]);
           $('#orders-content-date').scrollLeft($(this).prev().prev().prev().offset().left);
           service.buidMyOrders();
           return false;
         }
       });
     });

     $('#set-today').click(function(){
        setCurrentDate();
        service.buidMyOrders();
     });

     $(document).on('click','.order .cancel-order',function(){
       let id=$(this).parent().parent().parent().parent().parent().parent().attr('data-order-id');
       $.ajax({
         type: 'POST',
         url: "http://localhost/foru/order/info",
         contentType:"application/json",
         data: JSON.stringify({id: id}),
         success : function(result){
           $('.cancel-delivery-content .products').empty();
            for(j in result.orders[0].order.orderLines){
              let skuId=result.orders[0].order.orderLines[j].productId;
              let elem='<div class="row m-0 col-12 p-2 product" data-order-id="'+result.orders[0].order.id+'">'+
                         '<div class="col-3 m-0 p-2">'+
                           '<img class="mx-auto d-block img-thumbnail" src="'+result.orders[0].products[skuId].imageLocation+'">'+
                         '</div>'+
                         '<div class="col-8 p-0">'+
                           '<div class="col-12 light">'+
                             result.orders[0].products[skuId].brand+
                           '</div>'+
                           '<div class="col-12 text-nowrap">'+
                             '<b>'+result.orders[0].products[skuId].name+'b>'+
                           '</div>'+
                           '<div class="col-12 light">'+
                             result.orders[0].products[skuId].measurement+
                           '</div>'+
                           '<div class="col-12 light">'+
                             'Qty : '+result.orders[0].order.orderLines[j].quantity+
                           '</div>'+
                           '<div class="col-12 light">'+
                             '<b>₹'+result.orders[0].order.orderLines[j].soldPrice+'</b>'+
                           '</div>'+
                         '</div>'+
                       '</div>';
                       $('.cancel-delivery-content .products').append(elem);
            }
            $('#order-cancel-delivery').modal('show');
            setTimeout(function(){ makeFillRemainSpace('.cancel-delivery-content .filler');}, 300);
         },
         error:function(){
           $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
              $("#ajax-error-alert").slideUp(500);
            });
         }
       });
     });

     $("#order-cancelled-success-alert").hide();
     $('#order-cancel-confirm-btn').click(function(){
       let id=$('.cancel-delivery-content .products .product').eq(0).attr('data-order-id');
       $.ajax({
         type: 'POST',
         url: "http://localhost/foru/order/cancel",
         contentType:"application/json",
         data: JSON.stringify({id: id, mobile: localStorage.getItem('mobile') }),
         success : function(result){
           if(result.status=='CANCELLED' || result.status=='REFUNDED'){
             $('#order-cancel-delivery').modal('hide');
             $("#order-cancelled-success-alert").fadeTo(2000, 500).slideUp(500, function() {
                $("#order-cancelled-success-alert").slideUp(500);
              });
              service.buidMyOrders();
           }
           else {
             $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                $("#ajax-error-alert").slideUp(500);
              });
           }
         },
         error:function(){
           $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
              $("#ajax-error-alert").slideUp(500);
            });
         }
       });
     });

     $('#orders-back').click(function(){
       $('#home').show();
       $('#orders,#accounting,#help').hide();
       $('.index-footer .footer-menu').removeClass('footer-menu-active');
       $('.index-footer .footer-menu').eq(0).addClass('footer-menu-active');
     });

     $('#cancel-delivery-back').click(function(){
       $('#order-cancel-delivery').modal('hide');
     });

     $('#order-invoice-sent-success-alert').hide();
     $("#order-delivery-more-options").click(function(){
       $("#order-invoice-sent-success-alert").fadeTo(2000, 500).slideUp(500, function() {
          $("#order-invoice-sent-success-alert").slideUp(500);
        });
     });

     $('#myaccount-orders-link').click(function(){
       $('#user-profile').modal('hide');
       $('.index-footer .footer-menu').eq(1).trigger('click')
        setTimeout(function(){ makeFillRemainSpace('.orders-content .filler');}, 300);
     });

     $('#myaccount-subscriptions-link').click(function(){
       $.ajax({
         type: 'POST',
         url: "http://localhost/foru/subscriptions/mine",
         contentType:"application/json",
         data: JSON.stringify({mobile: localStorage.getItem('mobile')}),
         success: function(resultData) {
           $('.my-subscriptions-content').empty();
           let subscriptionLines=resultData.subscriptionLines;
           for(i in subscriptionLines){
                  let elem='<div class="section p-2" data-subscription-id="'+subscriptionLines[i].subscription.id+'">'+
                                '<div class="row m-0 col-12 p-2 hr">'+
                                    'Subscription : <b>'+subscriptionLines[i].subscription.frequency+'</b>';
                  if(subscriptionLines[i].subscription.preferredDays.length>0)
                        elem=elem+' - '+ subscriptionLines[i].subscription.preferredDays.join(',');

                  let subscribeItemIndex=0;
                  for(j in subscriptionLines[i].products){
                    let skuId=subscriptionLines[i].products[j].id;
                    elem=elem+'</div>'+
                        '<div class="row m-0 col-12 p-2 hr">'+
                          '<div class="col-3 m-0 p-2">'+
                            '<img class="mx-auto d-block img-thumbnail" src="'+subscriptionLines[i].products[skuId].imageLocation+'">'+
                          '</div>'+
                          '<div class="col-8 p-0">'+
                            '<div class="col-12 light">'+subscriptionLines[i].products[skuId].brand+
                            '</div>'+
                            '<div class="col-12 text-nowrap">'+
                              '<b>'+subscriptionLines[i].products[skuId].name+' </b>'+
                            '</div>'+
                            '<div class="col-12 light">'+subscriptionLines[i].products[skuId].measurement+
                            '</div>'+
                            '<div class="col-12 light">'+
                              'Qty : '+subscriptionLines[i].subscription.products[subscribeItemIndex++].quantity+
                            '</div>'+
                            '<div class="col-12 light">'+
                              '<b>₹'+subscriptionLines[i].products[skuId].currentPrice+'</b>'+
                            '</div>'+
                          '</div>';

                          if(subscriptionLines[i].subscription.status=='MODIFIED'){
                            let startDate =moment(subscriptionLines[i].subscription.modifiedSubscription.startDate).format('ddd, MMM D YYYY');
                            let endDate = moment(subscriptionLines[i].subscription.modifiedSubscription.endDate).format('ddd, MMM D YYYY');
                            let quantity=0;
                            for(k in subscriptionLines[i].subscription.modifiedSubscription.modifiedProducts){
                              if(subscriptionLines[i].subscription.modifiedSubscription.modifiedProducts[k].productId==skuId)
                                quantity=subscriptionLines[i].subscription.modifiedSubscription.modifiedProducts[k].quantity;
                            }
                            elem=elem+'<div class="row m-0 col-12 p-3 mx-auto modified-notification">'+
                                  '<div class="col-12 p-0">'+
                                    'Your subscription modified between <span class="start-date-changed">'+startDate+'</span> and <span class="end-date-changed">'+endDate+'</span>.'+
                                  '</div>'+
                                  '<div class="col-12 p-0 m-2 light">'+
                                    'quantity : '+quantity+
                                  '</div>'+
                                '</div>';
                          }
                        elem=elem+'</div>';
                  }
                    if(subscriptionLines[i].subscription.status=='PAUSED'){
                      elem=elem+'<div class="row m-0 col-12 p-2 mysubscriptions-btns">'+
                                  '<div class="col-12 p-0 text-right">'+
                                    '<button type="button" class="btn delete">'+
                                      '<i class="fas fa-trash-alt"></i>  DELETE'+

                                    '</button><button type="button" class="btn resume">'+
                                      '<i class="far fa-pause-circle"></i>  RESUME'+
                                    '</button>'+
                                  '</div>'+
                                '</div>';
                    }
                    else {
                      elem=elem+'<div class="row m-0 col-12 p-2 mysubscriptions-btns">'+
                          '<div class="col-12 p-0 text-right">'+
                            '<button type="button" class="btn modify">'+
                              '<i class="fas fa-calendar-alt"></i>  MODIFY'+

                            '</button><button type="button" class="btn delete">'+
                              '<i class="fas fa-trash-alt"></i>  DELETE'+

                            '</button><button type="button" class="btn pause">'+
                              '<i class="far fa-pause-circle"></i>  PAUSE'+
                            '</button>'+
                          '</div>'+
                        '</div>'+
                      '</div>';
                    }
                $('.my-subscriptions-content').append(elem);
           }
         },
         error: function(resultData) {
           $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
              $("#ajax-error-alert").slideUp(500);
            });
          }
       });
       $('#my-subscriptions').modal('show');
       setTimeout(function(){ makeFillRemainSpace('.my-subscriptions-content .filler');}, 300);
     });

     $('#myaccount-accounting-link').click(function(){
       $('#user-profile').modal('hide');
       $('.index-footer .footer-menu').eq(2).trigger('click')
     });

     $('#accounting-back').click(function(){
       $('.index-footer .footer-menu').eq(0).trigger('click')
     });

     $('#account-pay').click(function(){
       let amount=$('#advance-money-input').val();
       if(amount.length>=1){
         amount=amount.toString();
         amount=amount.includes('₹')?amount.split('₹')[1]:amount;
         service.payAccount(amount,"Recharge");
       }
     });

     $('#accounting-resrve-money-btn').click(function(){
       let amount=$('#accounting-resrve-money-input').val();
       if(amount.length>=1){
         amount=amount.toString();
         amount=amount.includes('₹')?amount.split('₹')[1]:amount;
         service.payAccount(amount,"Recharge");
       }
     });

     $('#mysubscriptions-back').click(function(){
       $('#my-subscriptions').modal('hide');
       $('#user-profile').modal('hide');
       $('.index-footer .footer-menu').eq(0).trigger('click')
     });

     $(document).on('click','.mysubscriptions-btns .modify',function(){
       let id=$(this).parent().parent().parent().attr('data-subscription-id');
       $('#mysubscriptions-modify-step1').attr('data-subscription-id',id);
       $('#mysubscriptions-modify-step1').modal('show');
     });

     $('.mysubscriptions-actions-footer').hide();
     $('.mysubscriptions-modify-temporary-btn').click(function(){
       let id=$('#mysubscriptions-modify-step1').attr('data-subscription-id');
       $('#mysubscriptions-action-dialog').attr('data-subscription-id',id);

       $.ajax({
              type: 'POST',
              url: "http://localhost/foru/subscriptions/get",
              contentType:"application/json",
              data: JSON.stringify({id: id}),
              success : function(result){
                $('.mysubscriptions-action-content .products').empty();
                for(i in result.subscription.products){
                  let skuId=result.subscription.products[i].productId;
                  let quantity=result.subscription.status=='MODIFIED'?result.subscription.modifiedSubscription.modifiedProducts[i].quantity:result.subscription.products[i].quantity;
                  let elem='<div class="row m-0 col-12 p-2 product" data-sku-id="'+skuId+'">'+
                              '<div class="col-3 m-0 p-2">'+
                                '<img class="mx-auto d-block img-thumbnail" src="'+result.products[skuId].imageLocation+'">'+
                              '</div>'+
                              '<div class="col-8 p-0">'+
                                '<div class="col-12 light">'+
                                  result.products[skuId].brand+
                                '</div>'+
                                '<div class="col-12 text-nowrap">'+
                                  '<b>'+result.products[skuId].name+'</b>'+
                                '</div>'+
                                '<div class="col-12 light">'+
                                  result.products[skuId].measurement+
                                '</div>'+
                                '<div class="col-12 light">'+
                                  'Qty : '+quantity+
                                '</div>'+
                                '<div class="row m-0 col-12">'+
                                  '<div class="col-6 p-0 light">'+
                                    '<b>₹'+result.products[skuId].currentPrice+'</b>'+
                                  '</div>'+
                                  '<div class="col-6 p-0 text-right my-subscriptions-actions-quantity-control">';
                      if(quantity>1)
                        elem=elem+'<i class="fas fa-minus-circle p-2"></i>';
                      else
                        elem=elem+'<i class="fas fa-minus-circle p-2 inactive"></i>';

                        elem=elem+'<span class="count">'+quantity+'</span>'+
                          '<i class="fas fa-plus-circle p-2"></i>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>';

                  $('.mysubscriptions-action-content .products').append(elem);
                }

                $('#mysubscriptions-modify-step1').modal('hide');
                $('#mysubscriptions-action-dialog').modal('show');
                $('.mysubscriptions-action-header .title').text('Modify Temporarily');
                $('.mysubscriptions-action-content .delete-area').hide();
                $('.mysubscriptions-action-content .update-perm-area').hide();
                $('.mysubscriptions-action-content .update-temp-area').show();
                $('.mysubscriptions-action-content .my-subscriptions-actions-quantity-control').show();
                setTimeout(function(){ makeFillRemainSpace('.mysubscriptions-action-content .filler');}, 300);
              },
              error:function(){
                $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                   $("#ajax-error-alert").slideUp(500);
                 });
              }
            });
     });

     $('.mysubscriptions-modify-permanent-btn').click(function(){

       let id=$('#mysubscriptions-modify-step1').attr('data-subscription-id');
       $('#mysubscriptions-action-dialog').attr('data-subscription-id',id);

       $.ajax({
              type: 'POST',
              url: "http://localhost/foru/subscriptions/get",
              contentType:"application/json",
              data: JSON.stringify({id: id}),
              success : function(result){
                $('.mysubscriptions-action-content .products').empty();
                for(i in result.subscription.products){
                  let skuId=result.subscription.products[i].productId;
                  let quantity=result.subscription.status=='MODIFIED'?result.subscription.modifiedSubscription.modifiedProducts[i].quantity:result.subscription.products[i].quantity;
                  let elem='<div class="row m-0 col-12 p-2 product" data-sku-id="'+skuId+'">'+
                              '<div class="col-3 m-0 p-2">'+
                                '<img class="mx-auto d-block img-thumbnail" src="'+result.products[skuId].imageLocation+'">'+
                              '</div>'+
                              '<div class="col-8 p-0">'+
                                '<div class="col-12 light">'+
                                  result.products[skuId].brand+
                                '</div>'+
                                '<div class="col-12 text-nowrap">'+
                                  '<b>'+result.products[skuId].name+'</b>'+
                                '</div>'+
                                '<div class="col-12 light">'+
                                  result.products[skuId].measurement+
                                '</div>'+
                                '<div class="col-12 light">'+
                                  'Qty : '+quantity+
                                '</div>'+
                                '<div class="row m-0 col-12">'+
                                  '<div class="col-6 p-0 light">'+
                                    '<b>₹'+result.products[skuId].currentPrice+'</b>'+
                                  '</div>'+
                                  '<div class="col-6 p-0 text-right my-subscriptions-actions-quantity-control">';
                      if(quantity>1)
                        elem=elem+'<i class="fas fa-minus-circle p-2"></i>';
                      else
                        elem=elem+'<i class="fas fa-minus-circle p-2 inactive"></i>';

                        elem=elem+'<span class="count">'+quantity+'</span>'+
                          '<i class="fas fa-plus-circle p-2"></i>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>';

                  $('.mysubscriptions-action-content .products').append(elem);
                }

                $('#mysubscriptions-modify-step1').modal('hide');
                $('#mysubscriptions-action-dialog').modal('show');
                $('.mysubscriptions-action-header .title').text('Modify Permanently');
                $('.mysubscriptions-action-content .delete-area').hide();
                $('.mysubscriptions-action-content .update-perm-area').show();
                $('.mysubscriptions-action-content .update-temp-area').hide();
                $('.mysubscriptions-action-content .my-subscriptions-actions-quantity-control').show();
                setTimeout(function(){ makeFillRemainSpace('.mysubscriptions-action-content .filler');}, 300);
              },
              error:function(){
                $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                   $("#ajax-error-alert").slideUp(500);
                 });
              }
            });

     });

     $(document).on('click', '.my-subscriptions-actions-quantity-control i', function(){
       let isMinus=$(this).hasClass('fa-minus-circle');
       let isActive=(!$(this).hasClass('inactive'));
       let count=parseInt($('.my-subscriptions-actions-quantity-control .count').text());
       if(isMinus && isActive){
         $('.my-subscriptions-actions-quantity-control .count').text(count-1);
         if(count-1 == 1)
           $(this).addClass('inactive');
       }
       else if(isActive){
         $('.my-subscriptions-actions-quantity-control .count').text(count+1);
         $('.my-subscriptions-actions-quantity-control i').removeClass('inactive');
       }
        $('.mysubscriptions-actions-footer').show();
     });

     $('.mysubscriptions-actions-footer .update-subscription').click(function(){
       let target='',successModal='',json='',data='';
       let startDate=$('#subscription-action-start-date').attr('value');
       let endDate=$('#subscription-action-end-date').attr('value');
       let id=$('#mysubscriptions-action-dialog').attr('data-subscription-id');
       let action=$('.mysubscriptions-action-header .title').text();
       switch (action) {
         case 'Modify Temporarily':
            target='http://localhost/foru/subscriptions/modify';
            successModal='mysubscriptions-modify-success';
            data={isPermanentChange:false,startDate:startDate,endDate:endDate,items:[],subscriptionId:id,mobile:localStorage.getItem('mobile')};
            $('.mysubscriptions-action-content .products .product').each(function(){
              let productItem=$(this).find('div');
              let skuId=$(this).attr('data-sku-id');
              let image=$(this).find('img').attr('src');
              let brand=productItem.find('div').eq(0).text();
              let name=productItem.find('div').eq(1).text();
              let measure=productItem.find('div').eq(2).text();
              let price=productItem.find('div').eq(4).text().split('₹')[1];
              let quantity=$('.my-subscriptions-actions-quantity-control').find('.count').text();
              data.items.push({brand:brand,image:image,measure:measure,name:name,price:price,skuId:skuId,quantity:quantity});
            });
            json=JSON.stringify({subscriptionBag:JSON.stringify(data)});
           break;
         case 'Modify Permanently':
            target='http://localhost/foru/subscriptions/modify';
            successModal='mysubscriptions-modify-success';
            data={isPermanentChange:true,startDate:startDate,endDate:endDate,items:[],subscriptionId:id,mobile:localStorage.getItem('mobile')};
            $('.mysubscriptions-action-content .products .product').each(function(){
              let productItem=$(this).find('div');
              let skuId=$(this).attr('data-sku-id');
              let image=$(this).find('img').attr('src');
              let brand=productItem.find('div').eq(1).text();
              let name=productItem.find('div').eq(2).text();
              let measure=productItem.find('div').eq(3).text();
              let price=productItem.find('div').eq(4).text().split('₹')[1];
              let quantity=$('.my-subscriptions-actions-quantity-control').find('.count').text();
              data.items.push({brand:brand,image:image,measure:measure,name:name,price:price,skuId:skuId,quantity:quantity});
            });
            json=JSON.stringify({subscriptionBag:JSON.stringify(data)});
           break;
           case 'Pause Subscription':
              json=JSON.stringify({id:id,startDate:startDate,endDate:endDate});
              target='http://localhost/foru/subscriptions/pause';
              successModal='mysubscriptions-pause-success';
            break;
       }


       $.ajax({
              type: 'POST',
              url: target,
              contentType:"application/json",
              data: json,
              success : function(result){
                if(result!==""){
                  $('#'+successModal).modal('show');
                }
              },
              error:function(){
                $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                   $("#ajax-error-alert").slideUp(500);
                 });
              }
            });


     });

     $('#my-subscriptions .my-subscriptions-content .section .modified-notification').hide();
     $('.mysubscriptions-modify-success-btn').click(function(){
       $('.mysubscriptions-actions-footer').hide();
       $('#mysubscriptions-modify-success').modal('hide');
       $('#mysubscriptions-action-dialog').modal('hide');
       $('#my-subscriptions').modal('show');
       $('#my-subscriptions .my-subscriptions-content .section .modified-notification').show();
       $('#myaccount-subscriptions-link').trigger('click');
     });

     $('#mysubscriptions-action-back').click(function(){
       $('.mysubscriptions-actions-footer').hide();
       $('#mysubscriptions-action-dialog').modal('hide');
     });

       $(document).on('click','.mysubscriptions-btns .delete',function(){
         let id=$(this).parent().parent().parent().attr('data-subscription-id');
         $('#mysubscriptions-delete-step1').attr('data-subscription-id',id);
         $('#mysubscriptions-delete-step1').modal('show');
       });

       $('#delete-subscription-other-reason').parent().hide();
       $('.mysubscriptions-delete-btn').click(function(){
         let id= $('#mysubscriptions-delete-step1').attr('data-subscription-id');
         $('#mysubscriptions-action-dialog').attr('data-subscription-id',id);
         $('#mysubscriptions-delete-step1').modal('hide');
         $('#mysubscriptions-action-dialog').modal('show');
         $('.mysubscriptions-action-header .title').text('Delete Subscription');
         $('.mysubscriptions-action-content .delete-area').show();
         $('.mysubscriptions-action-content .update-perm-area').hide();
         $('.mysubscriptions-action-content .update-temp-area').hide();
         $('.mysubscriptions-action-content .my-subscriptions-actions-quantity-control').hide();
         setTimeout(function(){ makeFillRemainSpace('.mysubscriptions-action-content .filler');}, 300);
       });

       $('input[type=radio][name=cancel-reason]').change(function() {
          if (this.value == 'Other reason') {
              $('#delete-subscription-other-reason').parent().show();
          }
          else{
            $('#delete-subscription-other-reason').parent().hide();
          }
      });

      $('#subscription-action-cancel').click(function(){
        let reasons=[];
        reasons.push($('input[type=radio][name=cancel-reason]').val());
        let id= $('#mysubscriptions-action-dialog').attr('data-subscription-id');

        $.ajax({
              type: 'POST',
              url: "http://localhost/foru/subscriptions/delete",
              contentType:"application/json",
              data: JSON.stringify({id: id,reasons:JSON.stringify(reasons)}),
              success : function(result){
                $('#mysubscriptions-action-dialog').modal('hide');
                $('#my-subscriptions').modal('show');
                $('#myaccount-subscriptions-link').trigger('click');
              },
              error:function(){
                $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                   $("#ajax-error-alert").slideUp(500);
                 });
              }
            });

      });

      $(document).on('click','.mysubscriptions-btns .pause,.mysubscriptions-pause-btn',function(){

        let id=$(this).parent().parent().parent().attr('data-subscription-id');
        if(!id)
          id=$('#mysubscriptions-delete-step1').attr('data-subscription-id');
        $('#mysubscriptions-action-dialog').attr('data-subscription-id',id);

        $.ajax({
               type: 'POST',
               url: "http://localhost/foru/subscriptions/get",
               contentType:"application/json",
               data: JSON.stringify({id: id}),
               success : function(result){
                 $('.mysubscriptions-action-content .products').empty();
                 for(i in result.subscription.products){
                   let skuId=result.subscription.products[i].productId;
                   let quantity=result.subscription.status=='MODIFIED'?result.subscription.modifiedSubscription.modifiedProducts[i].quantity:result.subscription.products[i].quantity;
                   let elem='<div class="row m-0 col-12 p-2 product" data-sku-id="'+skuId+'">'+
                               '<div class="col-3 m-0 p-2">'+
                                 '<img class="mx-auto d-block img-thumbnail" src="'+result.products[skuId].imageLocation+'">'+
                               '</div>'+
                               '<div class="col-8 p-0">'+
                                 '<div class="col-12 light">'+
                                   result.products[skuId].brand+
                                 '</div>'+
                                 '<div class="col-12 text-nowrap">'+
                                   '<b>'+result.products[skuId].name+'</b>'+
                                 '</div>'+
                                 '<div class="col-12 light">'+
                                   result.products[skuId].measurement+
                                 '</div>'+
                                 '<div class="col-12 light">'+
                                   'Qty : '+quantity+
                                 '</div>'+
                                 '<div class="row m-0 col-12">'+
                                   '<div class="col-6 p-0 light">'+
                                     '<b>₹'+result.products[skuId].currentPrice+'</b>'+
                                   '</div>'+
                                   '<div class="col-6 p-0 text-right my-subscriptions-actions-quantity-control">';
                       if(quantity>1)
                         elem=elem+'<i class="fas fa-minus-circle p-2"></i>';
                       else
                         elem=elem+'<i class="fas fa-minus-circle p-2 inactive"></i>';

                         elem=elem+'<span class="count">'+quantity+'</span>'+
                           '<i class="fas fa-plus-circle p-2"></i>'+
                         '</div>'+
                       '</div>'+
                     '</div>'+
                   '</div>';

                   $('.mysubscriptions-action-content .products').append(elem);
                 }

                 $('#mysubscriptions-delete-step1').modal('hide');
                 $('#mysubscriptions-action-dialog').modal('show');
                 $('.mysubscriptions-action-header .title').text('Pause Subscription');
                 $('.mysubscriptions-action-content .delete-area').hide();
                 $('.mysubscriptions-action-content .update-temp-area').show();
                 $('.mysubscriptions-action-content .my-subscriptions-actions-quantity-control').hide();
                 $('.mysubscriptions-actions-footer').show();
                 setTimeout(function(){ makeFillRemainSpace('.mysubscriptions-action-content .filler');}, 300);
               },
               error:function(){
                 $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                    $("#ajax-error-alert").slideUp(500);
                  });
               }
             });

      });

      $('.mysubscriptions-pause-success-btn').click(function(){
        $('#mysubscriptions-pause-success').modal('hide');
        $('#mysubscriptions-action-dialog').modal('hide');
        $('.mysubscriptions-actions-footer').hide();
        $('#my-subscriptions').modal('show');
        $('#my-subscriptions .my-subscriptions-content .section .modified-notification').show();
        $('#myaccount-subscriptions-link').trigger('click');
      });

      $('#subscription-resume-success-alert').hide();

      $(document).on('click','.mysubscriptions-btns .resume',function(){
        let id=$(this).parent().parent().parent().attr('data-subscription-id');
        $.ajax({
                type: 'POST',
                url: "http://localhost/foru/subscriptions/resume",
                contentType:"application/json",
                data: JSON.stringify({id: id}),
                success : function(result){
                  $("#subscription-resume-success-alert").fadeTo(2000, 500).slideUp(500, function() {
                     $("#subscription-resume-success-alert").slideUp(500);
                   });
                   $('#myaccount-subscriptions-link').trigger('click');
                },
                error:function(){
                  $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                     $("#ajax-error-alert").slideUp(500);
                   });
                }
              });
      });

      $('#subscription-action-start-date').val(moment(new Date()).format("YYYY-MM-DD"));
      $('#subscription-action-start-date').parent().siblings('div').find('.from-date').text(moment(new Date()).format("ddd, MMM D YYYY"));
      $('#subscription-action-start-date').bootstrapMaterialDatePicker({
         weekStart : 0,
         time: false,
         minDate : new Date(),
         maxDate : new Date(new Date().setMonth(new Date().getMonth()+1))
       }).on('change',function(e, date){
         let dateInfo=date._d.toString().split(' ');
         let displayDate=dateInfo[0]+' , '+dateInfo[1]+' '+dateInfo[2]+' '+dateInfo[3];
         $('.mysubscriptions-action-content .from-date').text(displayDate);
       });

       $('#subscription-action-end-date').val(moment(new Date()).format("YYYY-MM-DD"));
       $('#subscription-action-end-date').parent().siblings('div').find('.to-date').text(moment(new Date()).format("ddd, MMM D YYYY"));
       $('#subscription-action-end-date').bootstrapMaterialDatePicker({
          weekStart : 0,
          time: false,
          minDate : new Date(),
          maxDate : new Date(new Date().setMonth(new Date().getMonth()+1))
        }).on('change',function(e, date){
          let dateInfo=date._d.toString().split(' ');
          let displayDate=dateInfo[0]+' , '+dateInfo[1]+' '+dateInfo[2]+' '+dateInfo[3];
          $('.mysubscriptions-action-content .to-date').text(displayDate);
        });

        /***Accounting scripts starts here **/
        $('.advance-money-btns button').click(function(){
          let btnText=$(this).text().replace('+ ','');
          $('#advance-money-input').val(btnText);
        });

        $('#accounting-payment-history').click(function(){
          $.ajax({
               type: 'POST',
               url: "http://localhost/foru/account/mine",
               contentType:"application/json",
               data: JSON.stringify({mobile: localStorage.getItem('mobile')}),
               success : function(myResult){

                 $('#accounting-payment-history-dialog .accounting-action-content').empty();
                 if($.isEmptyObject(myResult.paymentHistories)){
                   $('#accounting-payment-history-dialog .accounting-action-content').append('<h6 class="row p-5 col-12 text-center">No payments are made yet.</h6>');
                 }else
                   for(i in myResult.paymentHistories){
                     let elem='<h6>'+i+'</h6>';
                     let histories=myResult.paymentHistories[i];
                     elem=elem+'<div class="section">';
                     for(j in histories){
                        (histories.length-1==j)?elem=elem+'<div class="row m-0 col-12 p-3">':elem=elem+'<div class="row m-0 col-12 p-3 hr">';
                        elem=elem+'<div class="col-8 p-0">'+
                                    '<div class="col-12 p-0 mb-2"> '+histories[j].name+' </div>'+
                                    '<div class="col-12 p-0 light"> Transaction Type : '+histories[j].transactionType+' </div>'+
                                  '</div>'+
                                  '<div class="col-4 p-0 text-right">'+
                                    '<div class="col-12 p-0 '+(histories[j].status=='Success'?'green':'red')+' mb-2"> '+histories[j].status+' </div>'+
                                    '<div class="col-12 p-0"> <b> ₹'+histories[j].paidAmount+' </b> </div>'+
                                  '</div>'+
                                '</div>';

                     }
                     elem=elem+'</div>';
                     $('#accounting-payment-history-dialog .accounting-action-content').append(elem);
                   }

                 $('#accounting-payment-history-dialog').modal('show');
                 setTimeout(function(){ makeFillRemainSpace('#accounting-payment-history-dialog .accounting-action-content .filler');}, 300);
               },
               error:function(){
                 $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                    $("#ajax-error-alert").slideUp(500);
                  });
               }
             });
        });

        $('#accounting-action-back').click(function(){
          $('#accounting-payment-history-dialog').modal('hide');
        });

        $('#accounting-pay-money').click(function(){
          $('#accounting-pay-money-dialog').modal('show');
          setTimeout(function(){ makeFillRemainSpace('.reserve-money-content .filler');}, 300);
        });

        $('#accounting-pay-money-dialog-back').click(function(){
          $('#accounting-pay-money-dialog').modal('hide');
        });

        $('#accounting-book').click(function(){
          $.ajax({
               type: 'POST',
               url: "http://localhost/foru/account/mine",
               contentType:"application/json",
               data: JSON.stringify({mobile: localStorage.getItem('mobile')}),
               success : function(myResult){
                  $('.account-book-content .data').empty();
                  if($.isEmptyObject(myResult.monthlyAccounting)){
                    $('.account-book-content').append('<h6 class="row p-5 col-12 text-center">No transactions are made yet.</h6>');
                  }else {
                      for(i in myResult.monthlyAccounting){
                        let elem='<h6 class="data">'+i+'</h6>';
                        let lines=myResult.monthlyAccounting[i].bookLines;
                        elem=elem+'<div class="section data">';
                        for(j in lines){
                              elem=elem+(lines.length-1==j?'<div class="row m-0 col-12 p-3">':'<div class="row m-0 col-12 p-3 hr">');
                              elem=elem+'<div class="col-3">'+moment(lines[j].eventDate).format('DD MMM')+'</div>'+
                                      '<div class="col-2">'+(lines[j].debit>0?'₹'+lines[j].debit:'')+'</div>'+
                                      '<div class="col-2">'+(lines[j].credit>0?'₹'+lines[j].credit:'')+'</div>'+
                                      '<div class="col-3">₹'+lines[j].balance+'</div>'+
                                      '<div class="col-2"><button class="btn details" data-detail="'+lines[j].detail+'">DETAILS</button></div>'+
                                    '</div>';
                        }
                        elem=elem+'<div class="row m-0 col-12 p-3 hr">'+
                                    '<div class="col-6 light">Closing Balance</div>'+
                                    '<div class="col-6 p-0 text-right"><b>₹'+myResult.monthlyAccounting[i].closingBalance+'</b></div>'+
                                  '</div>';
                        elem=elem+'<div class="row m-0 col-12 p-3">'+
                                    '<div class="col-6 light">Opening Balance</div>'+
                                    '<div class="col-6 p-0 text-right"><b>₹'+myResult.monthlyAccounting[i].openingBalance+'</b></div>'+
                                  '</div>';
                        elem=elem+'</div>';
                        $('.account-book-content').append(elem);
                      }
                  }
                  $('.account-book-balance').text('₹'+myResult.balance);
                  $('#accounting-account-book').modal('show');
                  setTimeout(function(){ makeFillRemainSpace('.account-book-content .filler');}, 300);
               },
               error:function(){
                 $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                    $("#ajax-error-alert").slideUp(500);
                  });
               }
             });


        });

        $('#accounting-account-book-back').click(function(){
          $('#accounting-account-book').modal('hide');
        });

        $('.account-book-content .details').click(function(){
          $('#myaccounting-account-transaction-deatils').modal('show');
        });

        $('.myaccounting-account-transaction-deatils-ok-btn').click(function(){
          $('#myaccounting-account-transaction-deatils').modal('hide');
        });

        $('#my-account-book-mail-sent').hide();
        $('.send-account-book').click(function(){
          $("#my-account-book-mail-sent").fadeTo(2000, 500).slideUp(500, function() {
             $("#my-account-book-mail-sent").slideUp(500);
           });
        });

        $('#help-back').click(function(){
          $('.index-footer .footer-menu').eq(0).trigger('click')
        });

        $('#help-action-back').click(function(){
          $('#help-action-dialog').modal('hide');
        });

        $('.help .help-menu').click(function(){
          let clickedText=$(this).find('div').eq(1).text().trim();
          switch (clickedText) {
            case 'Your Feedback':
              $('#help-action-dialog').modal('show');
              $('.your-feedback').show();
              $('.write-to-us,.call-us').hide();
              $('.help-action-header .title').text('Your Feedback');
              break;
            case 'Write to us':
              $('#help-action-dialog').modal('show');
              $('.write-to-us').show();
              $('.your-feedback,.call-us').hide();
              $('.help-action-header .title').text('Write to Us');
              break;
            case 'Talk to us':
              $('#help-action-dialog').modal('show');
              $('.call-us').show();
              $('.your-feedback,.write-to-us').hide();
              $('.help-action-header .title').text('Talk to Us');
              break;
          }
          setTimeout(function(){ makeFillRemainSpace('.help-action-content .filler');}, 300);
        });


        $('.login-user-menu a').click(function(){
          let menuHeaderText=$(this).find('span').text();
          switch (menuHeaderText) {
            case 'My Account':
              $('#user-menu').modal('hide');
              $('#user-profile').modal('show');
              break;
            case 'My Accounting':
              $('#user-menu').modal('hide');
              $('.index-footer .footer-menu').eq(2).trigger('click');
              break;
            case 'Shop By catagory':
              $('#user-menu').modal('hide');
              $('#shop-by-catagory').modal('show');
              setTimeout(function(){ makeFillRemainSpace('.catagories-content .filler');}, 300);
              break;
            case 'Need Help?':
              $('#user-menu').modal('hide');
              $('.index-footer .footer-menu').eq(3).trigger('click');
            break;
            case 'Logout':
              service.logout();
              break;
          }
        });

        $('#share-app,.share-app').click(function(){
          window.plugins.socialsharing.shareWithOptions(SocialShareOptions);
        });


        //search funtionalities
        $('#search-text').focus(function(){
          $('#search').show();
          $('#search .search-suggestions').hide();
          $('#search .search-results').hide().removeClass('d-flex');
          setTimeout(function(){$('#search-input-text').focus();}, 300);
        });

        $('#product-list-search').click(function(){
          $('#product-list').modal('hide');
          $('#search').show();
          $('#search .search-suggestions').hide();
          $('#search .search-results').hide().removeClass('d-flex');
          setTimeout(function(){$('#search-input-text').focus();}, 300);
        });

        $('#search-dialog-back').click(function(){
          $('#search').hide();
          $('input#search-input-text').val('');
        });

        $('input#search-input-text').on('input',function(e){
          let searchedText=$(this).val();
          if(searchedText==""){
            $('#search .search-suggestions').hide();
            $('#search .search-results').hide().removeClass('d-flex');
          }
          else{
            $.ajax({
              type: 'GET',
              url: "http://localhost/foru/search/suggest",
              contentType:"application/json",
              data: {text: searchedText},
              success : function(result){
                $('#search .search-suggestions').empty();
                for(i in result.categories){
                  let elemData=JSON.stringify(result.categories[i]).replaceAll("\"","'");
                  let elem='<div class="row col-12 m-0 suggestion-item" data-item="'+elemData+'">'+
                            '<div class="col-2">'+
                              '<i class="fas fa-search"></i>'+
                            '</div>'+
                            '<div class="col-8">'+result.categories[i].name+'</div>'+
                            '<div class="col-2 text-right">'+
                              '<i class="fas fa-angle-right"></i>'+
                            '</div>'+
                        '</div>';
                  $('#search .search-suggestions').append(elem);
                }
                for(i in result.products){
                  let elemData=JSON.stringify(result.products[i]).replaceAll("\"","'");
                  let elem='<div class="row col-12 m-0 suggestion-item" data-item="'+elemData+'">'+
                            '<div class="col-2">'+
                              '<i class="fas fa-search"></i>'+
                            '</div>'+
                            '<div class="col-8">'+result.products[i].name+'</div>'+
                            '<div class="col-2 text-right">'+
                              '<i class="fas fa-angle-right"></i>'+
                            '</div>'+
                        '</div>';
                  $('#search .search-suggestions').append(elem);
                }
                $('#search .search-suggestions').show();
                $('#search .search-results').hide().removeClass('d-flex');
              },
              error:function(){
                $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                   $("#ajax-error-alert").slideUp(500);
                 });
              }
            });

          }
          setTimeout(function(){
            makeFillRemainSpace($('.search-suggestions .filler'));
            makeFillRemainSpace($('.search-results .filler'));}, 300);
        });

        $(document).on("click","#search .search-suggestions .suggestion-item",function(){
          let searchitem=$(this).data('item').replaceAll("'","\"");
          let searchText=$(this).find('div').eq(1).text();
          $('input#search-input-text').val(searchText);
          $.ajax({
            type: 'POST',
            url: "http://localhost/foru/search/results",
            contentType:"application/json",
            data: JSON.stringify({selected: searchitem}),
            success : function(result){
              $('#search .search-results').empty();
              let products=result.products;
              for(i in products){
                let elem='<div class="col-6 product-item mr-auto" data-sku-id="'+products[i].id+'" data-sub-catagory="'+products[i].subCategory+'">'+
                            '<div class="col-12">'+
                              '<img class="mx-auto d-block img-thumbnail" src="'+products[i].imageLocation+'">'+
                            '</div>'+
                            '<div class="mb-1 row col-12 light">'+products[i].brand+'</div>'+
                            '<div class="mb-1 row col-12 dark">'+products[i].name+'</div>'+
                            '<div class="mb-1 row col-12 light">'+products[i].measurement+'</div>'+
                            '<div class="mb-1 row col-12 dark">₹'+products[i].currentPrice+'</div>';

                if(products[i].subscriptionAvailability){
                  elem=elem+'<div class="mb-1 row col-12 product-item-btns-holder">'+
                              '<button type="button" class="btn btn-success btn-subscribe">Subscribe @ ₹'+products[i].subscriptionPrice+'</button>'+
                            '</div>';
                }

                if(products[i].availableQuantity > 0){
                  elem=elem+'<div class="row col-12 product-item-btns-holder">'+
                              '<button type="button" class="btn btn-light btn-buy-once">Buy Once</button>'+
                            '</div>'+
                            '<div class="col-12 text-center buy-once-prod-quantity-control just-border" style="display: none;">'+
                              '<i class="fas fa-minus-circle ml-2 p-2"></i>'+
                              '<span class="count">1</span>'+
                              '<i class="fas fa-plus-circle  ml-2 p-2"></i>'+
                            '</div>';
                }
                else{
                  elem=elem+'<div class="mb-1 row col-12">'+
                              '<button type="button" class="btn w-100 out-of-stock">Out of Stock</button>'+
                            '</div>';
                }
                elem=elem+'</div>';

                $('#search .search-results').append(elem);
              }
              $('.buy-once-prod-quantity-control').hide();
              $('#search .search-suggestions').hide();
              $('#search .search-results').show().addClass('d-flex');
            },
            error:function(){
              $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                 $("#ajax-error-alert").slideUp(500);
               });
            }
          });

          setTimeout(function(){
            makeFillRemainSpace($('.search-suggestions .filler'));
            makeFillRemainSpace($('.search-results .filler'));}, 300);
        });

        //address update
        $('.delivery-location,.my-account-delivery-address').click(function(){
          $('#user-menu').modal('hide');
          $('#address-update-dialog').modal('show');
          $('.address-update-header .title').text('Select City');

          $('.select-city').show();
          $('.select-society,.selected-address,.request-scoiety').hide();
        });

        $('.select-city .city,.change-scoiety').click(function(){
          $.ajax({
            type: 'POST',
            url: "http://localhost/foru/profile/isVerified",
            contentType:"application/json",
            data: JSON.stringify({mobile: localStorage.getItem('mobile')}),
            success : function(result){
              if(result){
                $('.select-society').show();
                $('.select-city,.selected-address,.request-scoiety').hide();
                $('.address-update-header .title').text('Select Your Society');
                setTimeout(function(){ makeFillRemainSpace('.address-update-cotent .filler');}, 300);
              }else{
                $('#address-update-dialog').modal('hide');
              }
            },
            error:function(){
              $("#ajax-error-alert").fadeTo(2000, 500).slideUp(500, function() {
                 $("#ajax-error-alert").slideUp(500);
               });
            }
          });
        });

        $('#request-for-service').click(function(){
          $('.request-scoiety').show();
          $('.select-city,.selected-address,.select-society').hide();
          $('.address-update-header .title').text('Society Address');
          setTimeout(function(){ makeFillRemainSpace('.address-update-cotent .filler');}, 300);
        });

        $('.select-society .apartment').click(function(){
          $('.selected-address').show();
          $('.select-city,.request-scoiet,.select-society').hide();
          $('.address-update-header .title').text('Address');
          setTimeout(function(){ makeFillRemainSpace('.address-update-cotent .filler');}, 300);
        });

        $('#address-update-back').click(function(){
          $('#address-update-dialog').modal('hide');
        });

        $('#my-profile-update').click(function(){
          $('#my-profile-dialog').modal('show');
          setTimeout(function(){ makeFillRemainSpace('.my-profile-cotent .filler');}, 300);
        });

        $('#profile-back').click(function(){
            $('#my-profile-dialog').modal('hide');
        });


        service.logedinManagement();
        service.buildHomeCategories();
        service.buildPromotions();
        service.buildHomeMilk();
        service.buildDeals();

     //to fill remaining height in position fixed.
     function makeFillRemainSpace(selector){
       $(selector).each(function(){
         let viewportHeight=$(window).height();
         let fillerTop=$(selector).offset().top;
         let fillerHeight=parseInt(viewportHeight-fillerTop);
         if(fillerHeight > 20 && fillerTop!=0){
           let innerCss='width:calc(100%);height:'+fillerHeight+'px;';
           $(selector).attr('style',innerCss);
           $(selector).parent().attr('style','overflow:hidden;');
         }
       });
     }

     $('#ajax-loading-dialog').modal({backdrop: 'static', keyboard: false})
      $(document).on({
        ajaxStart: function(){
            $('#ajax-loading-dialog').modal('show');
        },
        ajaxStop: function(){
            setTimeout(function(){ $('#ajax-loading-dialog').modal('hide');}, 500);
        }
    });
});
