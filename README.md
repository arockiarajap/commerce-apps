The commerce incudes to manage the ecommerce for the existing shops. 

1. we make sure that we are helping their existing business as it is.
2. we help them to come online as easy in a single click.
3. We have to help them to move away from existing products.

Business cases :

1. The shops can install all the requred apps in a single place in a week max such as payroll, employee management, customer management, vendors 
   managment, accounting, performance anaysis, connect online , internal management, mail client.
2. The home town shops can be accessed via amazon and flipkart.
3. The money can be less for maintenance.
4. The apps can be used as service, as separate system.

The planned systems

S.No	Feature	                                     Comment	       Done?
1.  Location restriction of app usage                                   No
2.  Self pickup at the store                                            No
3.  Customer side - Android app	Subscrition & cart	                    Yes 
4.  Admin side - Android app		                                    No
5.	Delivery management		                                            No
6.	Googe sheets integration		                                    No
7.	Image Preparation through UI for promotions etc..	                No
8.	Prediction 		                                                    No
9.	Customer side - IOS app	Subscrition & cart	                        No
10.	Admin side - IOS app		                                        No
11.	Customer side - Web app	Subscrition & cart	                        No
12.	Admin side - Web app		                                        No
13.	Sent web link message to orders creation		                    No
14.	Whastapp integration		                                        No
15.	Amazon integration		                                            No
16.	Flipkart integration		                                        No
17.	Facebook integration		                                        No
18.	Twitter integration		                                            No
19.	Point of sales	Physcial store sales	                            No
20.	Attendance	Physical store	                                        No
21.	Employee management	Physical store	                                No
22.	Performance mangment	Physical store	                            No
23.	Payroll	Physical store	                                            No
24.	Customer segmentation and managment	Physical store	                No
25.	Vendors Management	Physical store	                                No
26.	Excel like product in web - with all features	Physical store	    No
27.	self billing	physical store	                                    No
28.	Report ( How much sales done)	physical store	                    No
29.	Sale by scan and manual entry	physical store	                    No
30.	Inventory ( what are there on which price and is there offers / 
    add products / edit products)	physical store	                    No
31.	Purchases (what orders from whom)	physical store	                No
32.	Shop info( Name and logo)	physical store	                        No
33.	Login via fingerprint and pin	physical store	                    No
34.	Admin to create customers, viewers of the data	physical store	    No

