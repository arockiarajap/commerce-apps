package com.h2flocks.entity

import com.arangodb.springframework.annotation.Document
import org.springframework.data.annotation.Id

@Document("promotions")
class Promotion extends Essential{
    @Id
    String Id

    String imageLocation
    PromotionGroup promotionGroup
    PromotionGroupType promotionGroupType
    boolean isCategory
    boolean isProduct
    boolean isInfo
    String categoryId
    String productId
}

enum PromotionGroup {
    HOME_FIRST_CAROUSEL,
    HOME_SECOND_CAROUSEL,
    HOME_BOTTOM
}

enum PromotionGroupType{
    SINGLE,
    CAROUSEL
}
