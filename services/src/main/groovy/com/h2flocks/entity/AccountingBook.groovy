package com.h2flocks.entity

import com.arangodb.springframework.annotation.Document
import org.springframework.data.annotation.Id

@Document("accountingBook")
class AccountingBook extends Essential{
    @Id
    String Id

    String profileId
    Float balance
    List<PaymentHistory> paymentHistories
    List<AccountBookLine> accountBookLines
}

class PaymentHistory{
    String name
    Date paymentDate
    String transactionType
    String status
    Long paidAmount
    Float balance
}

class AccountBookLine{
    Date eventDate
    Float debit
    Float credit
    Float balance
    AccountBookLineDetail detail
}

class AccountBookLineDetail{
    Date deliveryDate
    String remarks
    Float serviceCharge
    Float savings
}