package com.h2flocks.entity

import com.arangodb.springframework.annotation.Document
import org.springframework.data.annotation.Id

@Document("productCategories")
class ProductCategory extends Essential{
    @Id
    String Id

    String name
    String imageLocation
    List<String> subCategory=[]
}

@Document("products")
class Product extends Essential{
    @Id
    String Id

    String title
    String imageLocation
    String brand
    String name
    Integer quantity
    Float currentPrice
    Float usualPrice
    Float purchasedPrice
    Integer availableQuantity
    Long productCategory
    List<String> subCategory=[]
    boolean subscriptionAvailability
    ProductDetail productDetail
    String measurement
    Float subscriptionPrice
}

class ProductDetail{
    String title
    String descriptionDetail
    List<ProductDetail> chapters=[]
}