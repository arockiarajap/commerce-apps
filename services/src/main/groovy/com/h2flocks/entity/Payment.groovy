package com.h2flocks.entity

import com.arangodb.springframework.annotation.Document
import org.springframework.data.annotation.Id

@Document("payment")
class Payment extends Essential{
    @Id
    String id

    String mobile

    String request

    PaymentResponse response

    boolean isOrder

    boolean isSubscription

    boolean isAccountBookPayment

    String orderId

    String subscriptionId

}

class PaymentResponse{

    String status

    String data

}
