package com.h2flocks.entity

import com.arangodb.springframework.annotation.Document
import org.springframework.data.annotation.Id

@Document("order")
class Order extends Essential{
    @Id
    String id

    String name
    OrderStatus status
    List<OrderLineItem> orderLines=[]
    Float total
    Float serviceCharge
    Float savings
    Float subTotal
    Date deliveryDate
    String profileId
    Date orderedDate
    Boolean isPaid
    OrderSource orderSource
    String sourceSubscriptionId
}

class OrderLineItem {
    String productId
    Integer quantity
    Date soldAt
    Float margin
    Float purchasedPrice
    Float soldPrice
    Float Savings
}

enum OrderStatus {
    YET_TO_BE_ORDERED,ORDERED,TO_BE_DELIVERED,CANCELLED,REFUNDED,DELIVERED,FAILED
}

enum OrderSource{
    CART,SUBSCRIPTION
}
