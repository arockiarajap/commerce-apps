package com.h2flocks.entity

import com.arangodb.springframework.annotation.Document
import org.springframework.data.annotation.Id

@Document("requested-locations")
class RequestedLocation extends Essential{
    @Id
    String Id

    String city
    String block
    String houseNo
    String societyName
    String pinCode

}
