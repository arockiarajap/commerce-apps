package com.h2flocks.entity

import com.arangodb.springframework.annotation.Document
import org.springframework.data.annotation.Id

@Document("delivery-locations")
class DeliveryLocation extends Essential{
    @Id
    String Id

    String city
    String state
    String area
    String societyName
    String pinCode
    List<String> blocks
}
