package com.h2flocks.entity

import com.arangodb.springframework.annotation.Document
import org.springframework.data.annotation.Id

@Document("profile")
class UserProfile extends Essential{
    @Id
    String id

    String firstName
    String lastName
    String email
    String mobile
    String block
    String houseNo
    String deliveryLocation
    boolean mobileVerified
    Alerts alerts
    boolean isCreditAllowed
    Otp userOtp=new Otp()
}

class Alerts{
    boolean  ringBell
    boolean whatsapp
    boolean sms
    boolean mail
    boolean push
}

class Otp{
    String userOtp
    String sessionId
    List<OtpResponse> otpSendResponse=[]
    List<OtpResponse> otpVerifyResponse=[]
}

class OtpResponse{
    String Status
    String Details
}