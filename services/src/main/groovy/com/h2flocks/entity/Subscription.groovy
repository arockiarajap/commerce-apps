package com.h2flocks.entity

import com.arangodb.springframework.annotation.Document
import com.h2flocks.dto.Period
import org.springframework.data.annotation.Id

@Document("subscription")
class Subscription extends Essential{
    @Id
    String Id

    String name
    SubscriptionStatus status
    List<SubscribingItem> products
    Long total
    Long serviceCharge
    Long savings
    Long subTotal
    Date startedDate
    Date lastOrderedDate
    String profileId
    Period frequency
    ModifiedSubscription modifiedSubscription=new ModifiedSubscription()
    PausedSubscription pausedSubscription=new PausedSubscription()
    DeletedSubscription deletedSubscription=new DeletedSubscription()
    List<String> preferredDays=[]
    Map<Date,ModifiedSubscription> modifiedHistories=[:]
    Map<Date,PausedSubscription> pausedHistories=[:]
}

class SubscribingItem{
    String productId
    Integer quantity
}

class ModifiedSubscription{
    Date startDate
    Date endDate
    List<SubscribingItem> modifiedProducts=[]
}

class PausedSubscription{
    Date startDate
    Date endDate
}

class DeletedSubscription{
    Date deletedAt
    List<String> reasons=[]
}

enum SubscriptionStatus {
    ACTIVE,MODIFIED,DELETED,PAUSED
}


