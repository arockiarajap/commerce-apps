package com.h2flocks.actors

import akka.actor.ActorRef
import akka.actor.ActorSystem
import com.h2flocks.dto.ActorEvents
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct
import java.time.Duration

@Component
class ScheduledJobs {

    Logger logger= LoggerFactory.getLogger(ScheduledJobs.class)

    @Autowired
    ActorSystem actorSystem

    @Autowired
    @Qualifier("ordersActor")
    ActorRef ordersActor

    @Autowired
    @Qualifier("subscriptionsActor")
    ActorRef subscriptionsActor

    @PostConstruct
    void scheduleJobs(){
        logger.info("Jobs are starting to schedule")

        //schedule job for refunding the cancelled orders - every 5 minutes
        actorSystem.scheduler.scheduleAtFixedRate(Duration.ZERO,Duration.ofMillis(300000), ordersActor, ActorEvents.OrderEvents.CANCEL_ORDER,actorSystem.dispatcher(),null)

        //schedule job for generate the subscription active orders - every 20 minutes
        actorSystem.scheduler.scheduleAtFixedRate(Duration.ZERO,Duration.ofMinutes(3), subscriptionsActor, ActorEvents.SubscriptionEvents.GENERATE_ACTIVE_ORDER,actorSystem.dispatcher(),null)

        //schedule job for generate the subscription modified orders - every 20 minutes
        actorSystem.scheduler.scheduleAtFixedRate(Duration.ZERO,Duration.ofMinutes(20), subscriptionsActor, ActorEvents.SubscriptionEvents.GENERATE_MODIFIED_ORDER,actorSystem.dispatcher(),null)

        //schedule job for activate the paused subscriptions - every 10 minutes
        actorSystem.scheduler.scheduleAtFixedRate(Duration.ZERO,Duration.ofMinutes(10), subscriptionsActor, ActorEvents.SubscriptionEvents.ACTIVATE_PAUSED_SUBSCRIPTIONS,actorSystem.dispatcher(),null)

        //schedule job for stopping the temporarily modified subscriptions - every 10 minutes
        actorSystem.scheduler.scheduleAtFixedRate(Duration.ZERO,Duration.ofMinutes(10), subscriptionsActor, ActorEvents.SubscriptionEvents.STOP_TEMPORARY_MODIFICATIONS,actorSystem.dispatcher(),null)

        logger.info("Jobs are scheduled")
    }
}
