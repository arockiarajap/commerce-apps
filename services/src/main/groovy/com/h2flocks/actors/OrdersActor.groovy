package com.h2flocks.actors

import akka.actor.AbstractActor
import akka.japi.pf.ReceiveBuilder
import com.h2flocks.dto.ActorEvents
import com.h2flocks.entity.Order
import com.h2flocks.service.OrderService
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class OrdersActor extends AbstractActor{

    Logger logger= LoggerFactory.getLogger(OrdersActor.class)

    OrderService orderService

    OrdersActor(OrderService orderService) {
        this.orderService=orderService
    }

    @Override
    AbstractActor.Receive createReceive() {
        return receiveBuilder().match(ActorEvents.OrderEvents.class,{
            switch (it){
                case ActorEvents.OrderEvents.CANCEL_ORDER:
                    List<Order> refundedOrders=orderService.refundCancelledOrders()
                    logger.info("The refunded orders at ${new Date()} : ${refundedOrders.toString()}")
                    break
                default:
                    logger.error("There are some unknown events are occured : $it")
                    break
            }
        }).build()
    }
}
