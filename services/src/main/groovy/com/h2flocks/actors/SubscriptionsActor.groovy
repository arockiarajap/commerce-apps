package com.h2flocks.actors

import akka.actor.AbstractActor
import com.h2flocks.dto.ActorEvents
import com.h2flocks.entity.Subscription
import com.h2flocks.service.SubscriptionService
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class SubscriptionsActor extends AbstractActor{

    Logger logger= LoggerFactory.getLogger(SubscriptionsActor.class)

    SubscriptionService subscriptionService

    SubscriptionsActor(SubscriptionService subscriptionService) {
        this.subscriptionService=subscriptionService
    }

    @Override
    AbstractActor.Receive createReceive() {
        return receiveBuilder().match(ActorEvents.SubscriptionEvents.class,{switch (it){
                case ActorEvents.SubscriptionEvents.GENERATE_ACTIVE_ORDER:
                    List<Subscription> subscriptions=subscriptionService.generateActiveOrders()
                    logger.info("The active subscriptions at ${new Date()} : ${subscriptions.toString()}")
                    break
                case ActorEvents.SubscriptionEvents.GENERATE_MODIFIED_ORDER:
                    List<Subscription> subscriptions=subscriptionService.generateModifiedOrders()
                    logger.info("The active subscriptions at ${new Date()} : ${subscriptions.toString()}")
                    break
                case ActorEvents.SubscriptionEvents.ACTIVATE_PAUSED_SUBSCRIPTIONS:
                    List<Subscription> subscriptions=subscriptionService.activatePaused()
                    logger.info("The activated subscriptions which were pasued at ${new Date()} : ${subscriptions.toString()}")
                    break
                case ActorEvents.SubscriptionEvents.STOP_TEMPORARY_MODIFICATIONS:
                    List<Subscription> subscriptions=subscriptionService.stopTemporaryModification()
                    logger.info("The activated subscriptions which were temporarily modified at ${new Date()} : ${subscriptions.toString()}")
                    break
                default:
                    logger.error("There are some unknown events are occured : $it")
                    break
            }
        }).build()
    }
}
