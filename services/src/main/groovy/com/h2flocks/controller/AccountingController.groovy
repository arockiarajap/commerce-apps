package com.h2flocks.controller

import com.h2flocks.dto.MyAccounting
import com.h2flocks.service.AccountingService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController()
@CrossOrigin(origins = "*")
@RequestMapping(value = "account",consumes = ["application/json"])
class AccountingController {

    Logger logger= LoggerFactory.getLogger(AccountingController.class)

    @Autowired
    AccountingService accountingService

    @RequestMapping(value = "mine",method = RequestMethod.POST)
    MyAccounting accountBook(@RequestBody Map<String, String> payload){
        String mobile=payload['mobile']
        logger.info("The retrival of account book, for mobile  : $mobile" )
        return accountingService.getUserAccounting(mobile)
    }

    @RequestMapping(value = "pay",method = RequestMethod.POST)
    MyAccounting accountPay(@RequestBody Map<String, String> payload){
        String mobile=payload['mobile']
        Float amount=Float.parseFloat(payload['amount'])
        String transactionType=payload['transactionType']
        logger.info("The payment of account book, for mobile  : $mobile , amount : $amount and transacation type : $transactionType" )
        return accountingService.payMoney(mobile,amount,transactionType)
    }

}
