package com.h2flocks.controller

import com.h2flocks.entity.Product
import com.h2flocks.entity.Promotion
import com.h2flocks.service.PromotionService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController()
@CrossOrigin(origins = "*")
@RequestMapping(value = "promotions",consumes = ["application/json"])
class PromotionController {

    Logger logger= LoggerFactory.getLogger(PromotionController.class)

    @Autowired
    PromotionService promotionService

    @RequestMapping(value = "fetch",method = RequestMethod.POST)
    List<Promotion> getAllCategories(){
        logger.info("the promotions are fetching" )
        return promotionService.getAllPromotions()
    }

    @RequestMapping(value = "deals",method = RequestMethod.POST)
    List<Product> getDeals(){
        logger.info("the deals are being collected" )
        return promotionService.getDeals()
    }
}
