package com.h2flocks.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.h2flocks.dto.ModifySubscriptionBag
import com.h2flocks.dto.MySubscriptionLine
import com.h2flocks.dto.MySubscriptions
import com.h2flocks.dto.SubscriptionBag
import com.h2flocks.entity.Order
import com.h2flocks.entity.Subscription
import com.h2flocks.entity.SubscriptionStatus
import com.h2flocks.service.SubscriptionService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController()
@CrossOrigin(origins = "*")
@RequestMapping(value = "subscriptions",consumes = ["application/json"])
class SubscriptionController {

    Logger logger= LoggerFactory.getLogger(SubscriptionController.class)

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    SubscriptionService subscriptionService

    @RequestMapping(value = "new",method = RequestMethod.POST)
    Subscription placeASubscription(@RequestBody Map<String, String> payload){
        SubscriptionBag subscriptionBag=objectMapper.readValue(payload['subscriptionBag'].toString(), SubscriptionBag.class)
        logger.info("The subscription request for the bag : $subscriptionBag , for mobile  : ${subscriptionBag.mobile}" )
        return subscriptionService.saveSubscription(subscriptionBag)
    }

    @RequestMapping(value = "pause",method = RequestMethod.POST)
    SubscriptionStatus pause(@RequestBody Map<String, String> payload){
        String subscriptionId=payload['id']
        String startDate=payload['startDate']
        String endDate=payload['endDate']
        logger.info("The subscription request for pausing id: $subscriptionId , from  : $startDate to :$endDate" )
        return subscriptionService.pause(subscriptionId,startDate,endDate)
    }

    @RequestMapping(value = "modify",method = RequestMethod.POST)
    SubscriptionStatus modify(@RequestBody Map<String, String> payload){
        ModifySubscriptionBag modifySubscriptionBag=objectMapper.readValue(payload['subscriptionBag'].toString(), ModifySubscriptionBag.class)
        logger.info("The subscription request for modify id: ${modifySubscriptionBag.subscriptionId} , request : $modifySubscriptionBag ")
        return subscriptionService.modify(modifySubscriptionBag)
    }

    @RequestMapping(value = "delete",method = RequestMethod.POST)
    SubscriptionStatus delete(@RequestBody Map<String, String> payload){
        String subscriptionId=payload['id']
        List<String> reasons =objectMapper.readValue(payload['reasons'].toString(),ArrayList.class)
        logger.info("The subscription request for deleting id: $subscriptionId , reasons : $reasons" )
        return subscriptionService.delete(subscriptionId,reasons)
    }

    @RequestMapping(value = "resume",method = RequestMethod.POST)
    SubscriptionStatus resume(@RequestBody Map<String, String> payload){
        String subscriptionId=payload['id']
        logger.info("The subscription request for resuming id: $subscriptionId" )
        return subscriptionService.resume(subscriptionId)
    }

    @RequestMapping(value = "mine",method = RequestMethod.POST)
    MySubscriptions getMyOrders(@RequestBody Map<String, String> payload){
        String mobile=payload['mobile']
        logger.info("The placed subscriptions for the mobile  : $mobile " )
        return subscriptionService.mySubscriptions(mobile)
    }

    @RequestMapping(value = "get",method = RequestMethod.POST)
    MySubscriptionLine getSubscription(@RequestBody Map<String, String> payload){
        String id=payload['id']
        logger.info("The subscription is been retrieving  : $id " )
        return subscriptionService.getSubscription(id)
    }
}
