package com.h2flocks.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.h2flocks.entity.Product
import com.h2flocks.entity.SearchProductView
import com.h2flocks.service.SearchService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController()
@CrossOrigin(origins = "*")
@RequestMapping(value = "search",consumes = ["application/json"])
class SearchController {

    Logger logger= LoggerFactory.getLogger(SearchController.class)

    @Autowired
    SearchService searchService

    @Autowired
    ObjectMapper objectMapper

    @RequestMapping(value = "suggest",method = RequestMethod.GET)
    Map<String,Object> getSuggestions(@RequestParam("text") String searchText){
        logger.info("The Search request suggestions ($searchText)" )
        return searchService.suggestions(searchText)
    }

    @RequestMapping(value = "results",method = RequestMethod.POST)
    Map<String,Object> getResults(@RequestBody Map<String, String> payload){
        SearchProductView selected=objectMapper.readValue(payload['selected'].toString(),SearchProductView.class)
        logger.info("The Search request  ($selected)" )
        return searchService.searchResults(selected)
    }
}
