package com.h2flocks.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.h2flocks.dto.Cart
import com.h2flocks.dto.MyOrders
import com.h2flocks.entity.Order
import com.h2flocks.entity.OrderStatus
import com.h2flocks.service.OrderService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController()
@CrossOrigin(origins = "*")
@RequestMapping(value = "order",consumes = ["application/json"])
class OrderController {

    Logger logger= LoggerFactory.getLogger(OrderController.class)

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    OrderService orderService

    @RequestMapping(value = "new",method = RequestMethod.POST)
    OrderStatus placeAnOrder(@RequestBody Map<String, String> payload){
        Cart cart=objectMapper.readValue(payload['cart'].toString(),Cart.class)
        String date=payload['deliveryAt']
        String mobile=payload['mobile']
        boolean isFinal=payload['isFinal']
        logger.info("The order placement for the cart : $cart , for mobile  : $mobile by targeting at $date, isplaced order : $isFinal" )
        return orderService.saveOrder(mobile,cart,isFinal,date)
    }

    @RequestMapping(value = "mine",method = RequestMethod.POST)
    MyOrders getMyOrders(@RequestBody Map<String, String> payload){
        String mobile=payload['mobile']
        String date=payload['date']
        logger.info("The placed orders for the mobile  : $mobile " )
        return orderService.myOrders(mobile,date)
    }

    @RequestMapping(value = "cancel",method = RequestMethod.POST)
    Order cancelOrder(@RequestBody Map<String, String> payload){
        String mobile=payload['mobile']
        String orderId=payload['id']
        logger.info("The cancel request order # $orderId for the mobile  : $mobile " )
        return orderService.cancelOrder(mobile,orderId)
    }

    @RequestMapping(value = "delivered",method = RequestMethod.POST)
    Order deliveredOrder(@RequestBody Map<String, String> payload){
        String mobile=payload['mobile']
        String orderId=payload['id']
        logger.info("The delivery of order # $orderId for the mobile  : $mobile " )
        return orderService.updateOrder(mobile,orderId,OrderStatus.DELIVERED)
    }

    @RequestMapping(value = "info",method = RequestMethod.POST)
    MyOrders getOrder(@RequestBody Map<String, String> payload){
        String orderId=payload['id']
        logger.info("The info of order # $orderId" )
        return orderService.getOrder(orderId)
    }

}
