package com.h2flocks.controller

import com.h2flocks.entity.UserProfile
import com.h2flocks.service.ProfileService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController()
@CrossOrigin(origins = "*")
@RequestMapping(value = "profile",consumes = ["application/json"])
class ProfileController {

    Logger logger= LoggerFactory.getLogger(ProfileController.class)

    @Autowired
    ProfileService profileService

    @RequestMapping(value = "new",method = RequestMethod.POST)
    boolean createNewUser(@RequestBody Map<String, String> payload){
        String mobile=payload['mobile']
        logger.info("The create new user request by phone :  ,  $mobile" )
        return profileService.createNewProfile(mobile)
    }

    @RequestMapping(value = "verify",method = RequestMethod.POST)
    boolean verifyProfile(@RequestBody Map<String, String> payload){
        String mobile=payload['mobile'],otp=payload['otp']
        logger.info("The otp ($otp) request by phone :  ,  $mobile" )
        return profileService.verifyOtp(mobile,otp)
    }

    @RequestMapping(value = "isVerified",method = RequestMethod.POST)
    boolean isVerified(@RequestBody Map<String, String> payload){
        String mobile=payload['mobile']
        logger.info("The authorization request by phone :  ,  $mobile" )
        return profileService.isVerified(mobile)
    }

    @RequestMapping(value = "logout",method = RequestMethod.POST)
    boolean logout(@RequestBody Map<String, String> payload){
        String mobile=payload['mobile']
        logger.info("The logging out :  ,  $mobile" )
        return profileService.logout(mobile)
    }

}