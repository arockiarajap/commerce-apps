package com.h2flocks.controller

import com.h2flocks.entity.Product
import com.h2flocks.entity.ProductCategory
import com.h2flocks.service.ProductService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController()
@CrossOrigin(origins = "*")
@RequestMapping(value = "product",consumes = ["application/json"])
class ProductController {

    Logger logger= LoggerFactory.getLogger(ProductController.class)

    @Autowired
    ProductService productService

    @RequestMapping(value = "categories",method = RequestMethod.POST)
    List<ProductCategory> getAllCategories(){
        logger.info("the categories of product are serving" )
        return productService.getAllCategories()
    }

    @RequestMapping(value = "category-products",method = RequestMethod.POST)
    Map<String,Object> getProductByCategory(@RequestBody Map<String, String> payload){
        String category=payload['category']
        logger.info("The products are responding for the category, $category" )
        return productService.getCategoryProducts(category)
    }
}
