package com.h2flocks.dto

import com.h2flocks.entity.Product
import com.h2flocks.entity.Subscription

class MySubscriptions {
    String mobile
    List<MySubscriptionLine> subscriptionLines=[]
}
class MySubscriptionLine{
    Map<String, Product> products=[:]
    Subscription subscription
}