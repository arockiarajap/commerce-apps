package com.h2flocks.dto

class SubscriptionBag {
    List<SubscriptionItem> items
    String startDate
    Float serviceCharge
    String mobile
    Period frequency
    List<String> preferredDays=[]
}

class SubscriptionItem{
    String brand
    String image
    String measure
    String name
    Float price
    Integer quantity
    String skuId
}

enum Period{
    DAILY,ALTERNATE_DAY,EVERY_3_DAYS,WEEKLY,MONTHLY
}