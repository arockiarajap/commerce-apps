package com.h2flocks.dto

class ActorEvents {
   static enum OrderEvents{
        CANCEL_ORDER
    }
    static enum SubscriptionEvents{
        GENERATE_ACTIVE_ORDER,GENERATE_MODIFIED_ORDER,ACTIVATE_PAUSED_SUBSCRIPTIONS,STOP_TEMPORARY_MODIFICATIONS
    }
}
