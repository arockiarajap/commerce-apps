package com.h2flocks.dto

class ModifySubscriptionBag {
    boolean isPermanentChange
    String startDate
    String endDate
    List<SubscriptionItem> items
    String subscriptionId
    String mobile
}
