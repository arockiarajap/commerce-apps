package com.h2flocks.dto

class Cart {
    Integer count
    Integer delivery
    Float subtotal
    List<CartItem> items
}

class CartItem{
    String brand
    String image
    String measure
    String name
    Float price
    Integer quantity
    String skuId
}