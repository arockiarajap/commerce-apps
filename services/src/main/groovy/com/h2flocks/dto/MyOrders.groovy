package com.h2flocks.dto

import com.h2flocks.entity.Order
import com.h2flocks.entity.OrderStatus
import com.h2flocks.entity.Product

class MyOrders {
    String mobile
    String date
    List<MyOrderLine> orders=[]
}

class MyOrderLine{
    String orderTitle
    String orderStatus
    Map<String,Product> products=[:]
    Order order

    void setOrderTitle(Order order){
        Date today=new Date()
        if(order.deliveryDate>today)
            this.orderTitle="""Tomorrow's Order"""
        else if(order.deliveryDate==today)
            this.orderTitle="""Today's Order"""
        else if(order.deliveryDate==today.minus(1))
            this.orderTitle="""Yesterday's Order"""
        else if(order.deliveryDate>=today.minus(7))
            this.orderTitle="""Week's Order"""
        else
            this.orderTitle="""Month's Order"""
    }

    void setOrderStatus(OrderStatus orderStatus){
        switch (orderStatus){
            case OrderStatus.TO_BE_DELIVERED:
            case  OrderStatus.ORDERED:
                this.orderStatus='To be delivered'
                break
            case OrderStatus.DELIVERED:
                this.orderStatus='Delivered'
                break
            case OrderStatus.REFUNDED:
                this.orderStatus='Refunded'
                break
            case OrderStatus.CANCELLED:
                this.orderStatus='Cancelled'
                break
            case OrderStatus.FAILED:
                this.orderStatus='Failed'
                break
        }
    }
}
