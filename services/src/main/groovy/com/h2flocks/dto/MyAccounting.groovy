package com.h2flocks.dto

import com.h2flocks.entity.AccountBookLine
import com.h2flocks.entity.PaymentHistory

class MyAccounting {
    Float balance
    Map<String,List<PaymentHistory>> paymentHistories
    Map<String, MonthlyAccounting> monthlyAccounting
    MyAccountingSummary myAccountingSummary
}

class MyAccountingSummary{
    Float lastPaidAmount
    Float balanceAfterLastPayment
    Float billsSinceLastPayment
    Float monthlyAverageExpense
}

class MonthlyAccounting{
    List<AccountBookLine> bookLines
    Float openingBalance
    Float closingBalance
}
