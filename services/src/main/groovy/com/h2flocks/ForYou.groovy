package com.h2flocks

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class ForYou {
     static void main(String[] args){
        SpringApplication.run(ForYou.class);
    }
}