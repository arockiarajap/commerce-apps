package com.h2flocks.config

import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import com.h2flocks.actors.OrdersActor
import com.h2flocks.actors.SubscriptionsActor
import com.h2flocks.service.OrderService
import com.h2flocks.service.SubscriptionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean

class ActorSystemConfig {

    @Autowired
    OrderService orderService

    @Autowired
    SubscriptionService subscriptionService

    @Bean
    ActorSystem actorSystem(){
        ActorSystem.create()
    }

    @Bean
    @Qualifier("ordersActor")
    ActorRef ordersActor(){
        actorSystem().actorOf(Props.create(OrdersActor,orderService),"OrdersActor")
    }

    @Bean
    @Qualifier("subscriptionsActor")
    ActorRef subscriptionsActor(){
        actorSystem().actorOf(Props.create(SubscriptionsActor,subscriptionService),"subscriptionsActor")
    }

}
