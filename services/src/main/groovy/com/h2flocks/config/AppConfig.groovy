package com.h2flocks.config

import com.fasterxml.jackson.databind.ObjectMapper
import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer
import org.springframework.web.client.RestTemplate

@Configuration
@ComponentScan(["com.h2flocks"])
class AppConfig extends ActorSystemConfig{
    @Bean
    static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer()
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    JsonSlurper jsonSlurper(){
        new JsonSlurper()
    }

    @Bean
    @Qualifier("otpRestTemplate")
    RestTemplate otpRestTemplate(){
        new RestTemplate()
    }

    @Bean
    @Qualifier("otpVerifyRestTemplate")
    RestTemplate otpVerifyRestTemplate(){
        new RestTemplate()
    }

    @Bean
    @Qualifier("initiatePaymentRestTemplate")
    RestTemplate initiatePaymentRestTemplate(){
        new RestTemplate()
    }

    @Bean
    ObjectMapper objectMapper(){
        new ObjectMapper()
    }

}
