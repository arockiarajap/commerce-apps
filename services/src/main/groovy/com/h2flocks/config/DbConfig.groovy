package com.h2flocks.config

import com.arangodb.ArangoDB
import com.arangodb.Protocol
import com.arangodb.springframework.annotation.EnableArangoRepositories
import com.arangodb.springframework.config.ArangoConfiguration
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
@EnableArangoRepositories(basePackages = ["com.h2flocks"])
class DbConfig implements ArangoConfiguration{

    @Value('${db.user}')
    String user

    @Value('${db.pwd}')
    String pwd

    @Value('${db.host}')
    String host

    @Value('${db.port}')
    Integer port

    @Value('${db.name}')
    String dbName

    @Override
     ArangoDB.Builder arango() {
        ArangoDB.Builder arango = new ArangoDB.Builder()
                .useProtocol(Protocol.HTTP_JSON)
                .host(host, port)
                .user(user)
                .password("")
        return arango
    }

    @Override
     String database() {
        return dbName
    }
}
