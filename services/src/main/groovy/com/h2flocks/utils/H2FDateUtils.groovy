package com.h2flocks.utils

import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.text.SimpleDateFormat

@Component
class H2FDateUtils {

    SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE")

    int getWeek(Date date){
        Calendar calendar=Calendar.getInstance()
        calendar.setTime(date)
        return calendar.get(Calendar.WEEK_OF_YEAR)
    }

    int getMonth(Date date){
        Calendar calendar=Calendar.getInstance()
        calendar.setTime(date)
        return calendar.get(Calendar.MONTH)
    }

    String getDay(Date date){
        return dayFormat.format(date)
    }

    int getDayNumber(Date date){
        Calendar calendar=Calendar.getInstance()
        calendar.setTime(date)
        return calendar.get(Calendar.DAY_OF_MONTH)
    }

}
