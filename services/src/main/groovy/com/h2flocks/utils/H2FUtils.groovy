package com.h2flocks.utils

import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

@Component
class H2FUtils {

    String saveFile(String whereToSave, MultipartFile image) throws Exception{
        String fileLocation=whereToSave + image.getOriginalFilename()
        byte[] bytes = image.getBytes()
        Path path = Paths.get(fileLocation)
        Files.write(path, bytes)
        return fileLocation
    }


}
