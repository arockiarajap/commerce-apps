package com.h2flocks.data

import com.arangodb.springframework.annotation.Query
import com.arangodb.springframework.repository.ArangoRepository
import com.h2flocks.entity.Product
import org.springframework.data.repository.query.Param

interface ProductRepository extends ArangoRepository<Product,String>{

    @Query("FOR doc IN products FILTER doc.name == @name RETURN doc")
    Iterable<Product> findByName(@Param("name") String name)

    @Query("FOR doc IN products FILTER doc.productCategory == CONCAT(@productCategory) RETURN doc")
    Iterable<Product> findByCategory(@Param("productCategory") String productCategory)

    @Query("For p IN products FILTER p.currentPrice <= p.usualPrice AND p.availableQuantity>0 SORT p.name LIMIT 20 RETURN p")
    Iterable<Product> grabDeals()
}