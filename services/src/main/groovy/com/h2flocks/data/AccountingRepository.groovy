package com.h2flocks.data

import com.arangodb.springframework.annotation.Query
import com.arangodb.springframework.repository.ArangoRepository
import com.h2flocks.entity.AccountingBook
import org.springframework.data.repository.query.Param

interface AccountingRepository extends ArangoRepository<AccountingBook,String> {

    @Query("FOR doc IN accountingBook FILTER doc.profileId == @profileId RETURN doc")
    Iterable<AccountingBook> findByProfileId(@Param("profileId") String profileId)

}
