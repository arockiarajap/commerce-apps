package com.h2flocks.data

import com.arangodb.springframework.annotation.Query
import com.arangodb.springframework.repository.ArangoRepository
import com.h2flocks.entity.ProductCategory
import org.springframework.data.repository.query.Param

interface ProductCategoryRepository extends ArangoRepository<ProductCategory,String> {

    @Query("FOR doc IN productCategories FILTER doc.name == @name RETURN doc")
    Iterable<ProductCategory> findByName(@Param("name") String name)

}