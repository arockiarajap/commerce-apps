package com.h2flocks.data

import com.arangodb.springframework.annotation.Query
import com.arangodb.springframework.repository.ArangoRepository
import com.h2flocks.entity.SearchProductView
import org.springframework.data.repository.query.Param

interface SearchRepository extends ArangoRepository<SearchProductView,String> {

    @Query("""FOR doc IN productsearch
                SEARCH ANALYZER(LIKE(doc.name, @text), "text_en")
              RETURN doc""")
    Iterable<SearchProductView> findByText(@Param("text") String text)

}