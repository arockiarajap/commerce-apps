package com.h2flocks.data

import com.arangodb.springframework.annotation.Query
import com.arangodb.springframework.repository.ArangoRepository
import com.h2flocks.entity.Order
import com.h2flocks.entity.Subscription
import com.h2flocks.entity.SubscriptionStatus
import org.springframework.data.repository.query.Param

interface SubscriptionsRespository extends ArangoRepository<Subscription,String> {

    @Query("FOR doc IN subscription FILTER doc.profileId == @profileId RETURN doc")
    Iterable<Subscription> findByProfileId(@Param("profileId") String profileId)

    @Query("FOR doc IN subscription FILTER doc.status == @status RETURN doc")
    Iterable<Subscription> findByStatus(@Param("status") SubscriptionStatus subscriptionStatus)

}
