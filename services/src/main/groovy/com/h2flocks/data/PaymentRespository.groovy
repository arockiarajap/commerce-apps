package com.h2flocks.data


import com.arangodb.springframework.repository.ArangoRepository
import com.h2flocks.entity.Payment

interface PaymentRespository extends ArangoRepository<Payment,String> {

}