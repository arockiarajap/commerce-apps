package com.h2flocks.data

import com.arangodb.springframework.annotation.Query
import com.arangodb.springframework.repository.ArangoRepository
import com.h2flocks.entity.Promotion
import com.h2flocks.entity.PromotionGroup
import org.springframework.data.repository.query.Param

interface PromotionRespository extends ArangoRepository<Promotion,String> {

    @Query("FOR doc IN promotions FILTER doc.promotionGroup == @promotionGroup RETURN doc")
    Iterable<Promotion> findByPromotionGroup(@Param("promotionGroup") String promotionGroup)

    @Query("FOR doc IN promotions FILTER doc.promotionGroupType == @promotionGroupType RETURN doc")
    Iterable<Promotion> findByPromotionGroupType(@Param("promotionGroupType") String promotionGroupType)

    @Query("FOR doc IN promotions FILTER doc.promotionGroupType == @promotionGroupType && doc.promotionGroup == @promotionGroup RETURN doc")
    Iterable<Promotion> findByPromotionGroupAndType(@Param("promotionGroupType") String promotionGroupType,@Param("promotionGroup") String promotionGroup)
}