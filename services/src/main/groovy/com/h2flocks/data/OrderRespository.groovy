package com.h2flocks.data

import com.arangodb.springframework.annotation.Query
import com.arangodb.springframework.repository.ArangoRepository
import com.h2flocks.entity.Order
import com.h2flocks.entity.OrderStatus
import org.springframework.data.repository.query.Param

interface OrderRespository extends ArangoRepository<Order,String> {

    @Query("FOR order in order SORT order.orderedDate DESC FILTER DATE_FORMAT(order.orderedDate,'%yyyy-%mm-%dd')==@orderDate AND order.profileId == @profileId RETURN order")
    Iterable<Order> findByProfileId(@Param("orderDate") String orderDate,@Param("profileId") String profileId)

    @Query("FOR order in order FILTER order.status == @status RETURN order")
    Iterable<Order> findByStatus(@Param("status") OrderStatus status)

    @Query("FOR order in order FILTER order.sourceSubscriptionId == @subscriptionId RETURN order")
    Iterable<Order> findBySubscriptionId(@Param("subscriptionId") String subscriptionId)

}
