package com.h2flocks.data

import com.arangodb.springframework.annotation.Query
import com.arangodb.springframework.repository.ArangoRepository
import com.h2flocks.entity.UserProfile
import org.springframework.data.repository.query.Param

interface ProfileRepository extends ArangoRepository<UserProfile,String> {

    @Query("FOR doc IN profile FILTER doc.mobile == @mobile RETURN doc")
    Iterable<UserProfile> findByMobile(@Param("mobile") String mobile)

    @Query("FOR doc IN profile RETURN doc")
    Iterable<UserProfile> findAllProfile()

}
