package com.h2flocks.service

import com.h2flocks.data.SubscriptionsRespository
import com.h2flocks.dto.ModifySubscriptionBag
import com.h2flocks.dto.MySubscriptionLine
import com.h2flocks.dto.MySubscriptions
import com.h2flocks.dto.Period
import com.h2flocks.dto.SubscriptionBag
import com.h2flocks.entity.Order
import com.h2flocks.entity.OrderStatus
import com.h2flocks.entity.PausedSubscription
import com.h2flocks.entity.Product
import com.h2flocks.entity.SubscribingItem
import com.h2flocks.entity.Subscription
import com.h2flocks.entity.SubscriptionStatus
import com.h2flocks.utils.H2FDateUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import java.text.Format
import java.text.SimpleDateFormat

@Service
class SubscriptionService {

    Logger logger= LoggerFactory.getLogger(SubscriptionService.class)

    SimpleDateFormat DAY_MONTH_YEAR_FORMAT=new SimpleDateFormat("yyyy-MM-dd")

    @Autowired
    SubscriptionsRespository subscriptionsRespository

    @Autowired
    ProfileService profileService

    @Autowired
    OrderService orderService

    @Autowired
    ProductService productService

    @Autowired
    H2FDateUtils dateUtils

    Subscription saveSubscription(SubscriptionBag subscriptionBag){
        try{
            List<SubscribingItem> subscribingItems=[]
            subscriptionBag.items.each {
                subscribingItems.add(new SubscribingItem(
                        productId :it.skuId,
                        quantity: it.quantity
                ))
            }

            String name=subscriptionBag.frequency.name()+' - '+subscribingItems.join(',')

            Float subtotal=0.0
            subscriptionBag.items.each {
                subtotal=subtotal+(productService.getProductRepository().findById(it.skuId).get().currentPrice*it.quantity)
            }

            Float serviceCharge=2

            Float savings=2

            Float total=subtotal+serviceCharge-savings

            Subscription subscription=new Subscription(
                    name :name,
                    status: SubscriptionStatus.ACTIVE,
                    products: subscribingItems,
                    total: total,
                    serviceCharge: serviceCharge,
                    savings: savings,
                    subTotal: subtotal,
                    startedDate: Date.parse("yyyy-MM-dd", subscriptionBag.startDate),
                    profileId: profileService.getUserprofile(subscriptionBag.mobile).id,
                    frequency: subscriptionBag.frequency,
                    preferredDays: subscriptionBag.preferredDays,
                    createdAt: new Date(),
                    modifiedAt: new Date()
            )

            subscriptionsRespository.save(subscription)
        }
        catch(Exception ex){
            logger.info("The error occured while saving the subscription $ex for the subscriptionBag $subscriptionBag")
        }

    }

    List<Subscription> generateActiveOrders(){
        try{
            generateOrders(SubscriptionStatus.ACTIVE)
        }
        catch(Exception ex){
            logger.info("The error occured while generateActiveOrders for the subscriptions $ex")
        }
    }

    List<Subscription> generateModifiedOrders(){
        try{
            generateOrders(SubscriptionStatus.MODIFIED)
        }
        catch(Exception ex){
            logger.info("The error occured while generateModifiedOrders for the subscriptions $ex")
        }
    }

    List<Subscription> activatePaused(){
        try {
            List<Subscription> subscriptions = []
            subscriptionsRespository.findByStatus(SubscriptionStatus.PAUSED).each {
                if (it.pausedSubscription.endDate <= new Date()) {
                    it.status = SubscriptionStatus.ACTIVE
                    it.pausedHistories.put(new Date(),it.pausedSubscription)
                    it.pausedSubscription.startDate = it.pausedSubscription.endDate = null
                    it.modifiedAt=new Date()
                    subscriptionsRespository.save(it)
                }
            }
            subscriptions
        }
        catch(Exception ex){
            logger.info("The error occured while activatePaused for the subscriptions $ex")
        }
    }

    List<Subscription> stopTemporaryModification(){
        try {
            List<Subscription> subscriptions = []
            subscriptionsRespository.findByStatus(SubscriptionStatus.MODIFIED).each {
                if (it.modifiedSubscription.endDate && it.modifiedSubscription.endDate <= new Date()) {
                    it.status = SubscriptionStatus.ACTIVE
                    it.modifiedHistories.put(new Date(),it.modifiedSubscription)
                    it.modifiedSubscription.startDate = it.modifiedSubscription.endDate = null
                    it.modifiedAt=new Date()
                    subscriptionsRespository.save(it)
                }
            }
            subscriptions
        }
        catch(Exception ex){
            logger.info("The error occured while stopModifiedAtLastDate for the subscriptions $ex")
        }
    }

    SubscriptionStatus pause(String subscriptionId, String startDate,String endDate){
        try{
            Date startAt = Date.parse("yyyy-MM-dd", startDate)
            Date endAt = Date.parse("yyyy-MM-dd", endDate)
            Subscription subscription=subscriptionsRespository.findById(subscriptionId).get()
            if(subscription.status!=SubscriptionStatus.PAUSED){
                subscription.pausedHistories.put(new Date(),subscription.pausedSubscription)
                subscription.pausedSubscription.startDate=subscription.pausedSubscription.endDate=null
            }
            subscription.status=SubscriptionStatus.PAUSED
            subscription.pausedSubscription.startDate=startAt
            subscription.pausedSubscription.endDate=endAt
            subscription.modifiedAt=new Date()

            subscriptionsRespository.save(subscription)

            //cancel order which was placed before pausing the subscription which happened today
            orderService.cancelSubscriptionOrderIfMatches(subscription)
            return subscription.status
        }
        catch(Exception ex){
            logger.info("The error occured while pausing for the subscription :$subscriptionId $ex")
        }
    }

    SubscriptionStatus modify(ModifySubscriptionBag modifySubscriptionBag){
        try{
            Subscription subscription=subscriptionsRespository.findById(modifySubscriptionBag.subscriptionId).get()
            subscription.modifiedHistories.put(new Date(),subscription.modifiedSubscription)
            if(modifySubscriptionBag.isPermanentChange){
                subscription.modifiedSubscription.startDate=subscription.modifiedSubscription.endDate=null
                subscription.products=[]
                modifySubscriptionBag.items.each {
                    subscription.products.add(new SubscribingItem(
                            productId: it.skuId,
                            quantity: it.quantity
                    ))
                }
                subscription.status=SubscriptionStatus.ACTIVE
            }else {
                subscription.modifiedSubscription.startDate=Date.parse("yyyy-MM-dd", modifySubscriptionBag.startDate)
                subscription.modifiedSubscription.endDate=Date.parse("yyyy-MM-dd", modifySubscriptionBag.endDate)
                modifySubscriptionBag.items.each {
                    subscription.modifiedSubscription.modifiedProducts.add(new SubscribingItem(
                            productId: it.skuId,
                            quantity: it.quantity
                    ))
                }
                subscription.status=SubscriptionStatus.MODIFIED
            }
            subscription.modifiedAt=new Date()
            subscriptionsRespository.save(subscription)

            //cancel order which was placed before modifying the subscription which happened today
            orderService.cancelSubscriptionOrderIfMatches(subscription)

            return subscription.status
        }catch(Exception ex){
            logger.info("The error occured while modifying for the subscription :$modifySubscriptionBag $ex")
        }
    }

    SubscriptionStatus delete(String subscriptionId, List<String> reasons){
        try{
            Subscription subscription=subscriptionsRespository.findById(subscriptionId).get()
            subscription.deletedSubscription.deletedAt=new Date()
            subscription.deletedSubscription.reasons=reasons
            subscription.status=SubscriptionStatus.DELETED

            subscription.modifiedAt=new Date()

            subscriptionsRespository.save(subscription)

            //cancel order which was placed before modifying the subscription which happened today
            orderService.cancelSubscriptionOrderIfMatches(subscription)

            return subscription.status
        }catch(Exception ex){
            logger.info("The error occured while deleteting the subscription :$subscriptionId $ex")
        }
    }

    SubscriptionStatus resume(String subscriptionId){
        try{
            Subscription subscription=subscriptionsRespository.findById(subscriptionId).get()
            subscription.pausedHistories.put(new Date(),subscription.pausedSubscription)
            subscription.pausedSubscription=new PausedSubscription()

            subscription.status=SubscriptionStatus.ACTIVE
            subscription.modifiedAt=new Date()

            subscriptionsRespository.save(subscription)

            return subscription.status
        }catch(Exception ex){
            logger.info("The error occured while resuming the subscription :$subscriptionId $ex")
        }
    }

    MySubscriptions mySubscriptions(String mobile){
        try{
            MySubscriptions mySubscriptions=new MySubscriptions(mobile:mobile)
            subscriptionsRespository.findByProfileId(profileService.getUserprofile(mobile).id).each {
                if(it.status!=SubscriptionStatus.DELETED){
                    Map<String,Product> products=[:]
                    it.products.each {
                        products.put(it.productId,productService.getProductRepository().findById(it.productId).get())
                    }
                    mySubscriptions.subscriptionLines.add(
                            new MySubscriptionLine(
                                    subscription: it,
                                    products: products
                            )
                    )
                }
            }

           return mySubscriptions
        }
        catch(Exception ex){
            logger.info("The error occured while listing my orders $ex")
            return [:]
        }
    }

    MySubscriptionLine getSubscription(String id){
    try{
        MySubscriptionLine mySubscriptionLine=new MySubscriptionLine(
                subscription: subscriptionsRespository.findById(id).get()
        )
        mySubscriptionLine.subscription.products.each {
            mySubscriptionLine.products.put(it.productId,productService.getProductRepository().findById(it.productId).get())
        }
        return mySubscriptionLine
    }
    catch(Exception ex){
        logger.info("The error occured while gettng the subscription $id, $ex")
        return null
    }
    }

    private List<Subscription> generateOrders(SubscriptionStatus subscriptionStatus){
        try{
            List<Subscription> subscriptions=[]
            Date today=Date.parse("yyyy-MM-dd",DAY_MONTH_YEAR_FORMAT.format(new Date()))
            subscriptionsRespository.findByStatus(subscriptionStatus).each {
                Date startedDate=Date.parse("yyyy-MM-dd",DAY_MONTH_YEAR_FORMAT.format(it.startedDate))
                Date lastOrderedDate=it.lastOrderedDate?Date.parse("yyyy-MM-dd",DAY_MONTH_YEAR_FORMAT.format(it.lastOrderedDate)):null
                Date createdAt=Date.parse("yyyy-MM-dd",DAY_MONTH_YEAR_FORMAT.format(it.createdAt))
                if(startedDate<=today &&  (lastOrderedDate<today || createdAt<today)){
                    Date deliveryAtTomorrow=today.plus(1)
                    OrderStatus orderStatus
                    switch (it.frequency){
                        case Period.DAILY:
                            if((lastOrderedDate==today.minus(1) || createdAt==today) && startedDate<=today)
                                orderStatus=orderService.subscriptionOrder(it,deliveryAtTomorrow)
                            break
                        case Period.ALTERNATE_DAY:
                            if((lastOrderedDate==today.minus(2) || createdAt==today) && startedDate<=today)
                                orderStatus=orderService.subscriptionOrder(it,deliveryAtTomorrow)
                            break
                        case Period.EVERY_3_DAYS:
                            if((lastOrderedDate==today.minus(3) || createdAt==today) && startedDate<=today)
                                orderStatus=orderService.subscriptionOrder(it,deliveryAtTomorrow)
                            break
                        case Period.WEEKLY:
                            if((dateUtils.getWeek(today)-dateUtils.getWeek(lastOrderedDate)==1 && it.preferredDays.contains(dateUtils.getDay(today))) || (dateUtils.getDayNumber(createdAt)-dateUtils.getDayNumber(today)<=7 && it.preferredDays.contains(dateUtils.getDay(today))))
                                orderStatus=orderService.subscriptionOrder(it,deliveryAtTomorrow)
                            break
                        case Period.MONTHLY:
                            if(dateUtils.getMonth(today)-dateUtils.getMonth(lastOrderedDate)==1 && it.preferredDays.contains(dateUtils.getDayNumber(today)) || (dateUtils.getDayNumber(createdAt)-dateUtils.getDayNumber(today)<=28 && it.preferredDays.contains(dateUtils.getDay(today))))
                                orderStatus=orderService.subscriptionOrder(it,deliveryAtTomorrow)
                            break
                    }
                    //set last ordered date
                    if(orderStatus && orderStatus!=OrderStatus.FAILED){
                        it.setLastOrderedDate(new Date())
                        subscriptionsRespository.save(it)
                    }
                }
            }
            subscriptions
        }
        catch(Exception ex){
            throw ex
        }
    }

}
