package com.h2flocks.service

import com.h2flocks.data.ProductRepository
import com.h2flocks.data.PromotionRespository
import com.h2flocks.entity.Product
import com.h2flocks.entity.Promotion
import com.h2flocks.entity.PromotionGroup
import com.h2flocks.entity.PromotionGroupType
import com.h2flocks.utils.H2FUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class PromotionService {

    Logger logger= LoggerFactory.getLogger(PromotionService.class)

    @Value('${promotion.image}')
    String promotionImages

    @Autowired
    PromotionRespository promotionRespository

    @Autowired
    ProductRepository productRepository

    @Autowired
    H2FUtils h2FUtils

    boolean createOrModifyPromotion(String Id, MultipartFile image, PromotionGroup promotionGroup, PromotionGroupType promotionGroupType, boolean isCategory,
                                    boolean isProduct, boolean isInfo, String categoryId, String productId){
        try{
            Promotion promotion
            if(Id){
                promotion=promotionRespository.findById(Id)
            }else{
                promotion=new Promotion(createdAt: new Date(),modifiedAt: new Date())
            }
            promotion.imageLocation=h2FUtils.saveFile(promotionImages,image)
            promotion.promotionGroup=promotionGroup
            promotion.promotionGroupType=promotionGroupType
            promotion.isCategory=isCategory
            promotion.isProduct=isProduct
            promotion.isInfo=isInfo
            promotion.categoryId=categoryId
            promotion.productId=productId
            promotion.modifiedAt=new Date()
            promotionRespository.save(promotion)!=null
        }
        catch(Exception ex){
            logger.info("The promotion couldn't be saved $ex")
            return false
        }
    }

    List<Promotion> getAllPromotions(){
        try{
            Iterable<Promotion> promotions=promotionRespository.findAll()
            return promotions.toList()
        }
        catch(Exception ex){
            logger.info("The promotion couldn't be deleted $ex")
            return []
        }
    }

    List<Promotion> getPromotionsByTypeAndGroup(PromotionGroupType promotionGroupType,PromotionGroup promotionGroup){
        try{
            List<Promotion> promotions=[]
            if(promotionGroupType){
                promotions=promotionRespository.findByPromotionGroupType(promotionGroupType.name()).toList()
            }else if(promotionGroup){
                promotions=promotionRespository.findByPromotionGroup(promotionGroup.name()).toList()
            }else if(promotionGroup && promotionGroupType){
                promotions=promotionRespository.findByPromotionGroupAndType(promotionGroupType.name(),promotionGroup.name()).toList()
            }
            return promotions
        }
        catch(Exception ex){
            logger.info("The promotion couldn't be deleted $ex")
            return []
        }
    }

    boolean deletePromotion(String id){
        try{
            promotionRespository.deleteById(id)
            return true
        }
        catch(Exception ex){
            logger.info("The promotion couldn't be deleted $ex")
            return false
        }
    }

    List<Product> getDeals(){
        try{
            List<Product> availDeals=productRepository.grabDeals().toList()
            return availDeals
        }
        catch(Exception ex){
            logger.info("The error occured while getting the products $ex")
            return []
        }
    }

}
