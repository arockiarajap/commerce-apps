package com.h2flocks.service

import com.h2flocks.data.SearchRepository
import com.h2flocks.entity.Product
import com.h2flocks.entity.ProductCategory
import com.h2flocks.entity.SearchProductView
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SearchService {

    @Autowired
    SearchRepository searchRepository

    @Autowired
    ProductService productService

    Logger logger= LoggerFactory.getLogger(SearchService.class)

    Map<String,Object> suggestions(String query){
        try{
            Iterable<SearchProductView> results=searchRepository.findByText(this.getSearchQuery(query))
            Map<String,Object> parsedResults=[:]
            List<Product> products=[]
            List<ProductCategory> categories=[]
            results.each {
                boolean isProduct= it.brand!=null
                if(isProduct){
                    products.add(this.buildProduct(it))
                }else{
                    categories.add(this.buildProductCategory(it))
                }
            }
            parsedResults.put("products",products)
            parsedResults.put("categories",categories)
            return parsedResults
        }
        catch(Exception ex){
            logger.info("The suggestions couldnt be retrieved $ex")
            return [:]
        }
    }

    Map<String,Object> searchResults(SearchProductView selected){
        try{
            Map<String,Object> results=[:]
            boolean isProduct= selected.brand!=null
            if(isProduct){
                results=productService.getCategoryProducts(selected.productCategory.toString())
                List<Product> products=results.get("products")
                products.plus(0,this.buildProduct(selected))
                results.put("products",products)
            }
            else{
                results=productService.getCategoryProducts(selected.Id.toString())
            }
            return results
        }
        catch(Exception ex){
            logger.info("The results couldn't be retrieved $ex")
            return [:]
        }
    }

    private String getSearchQuery(String userInput){
        return new StringBuilder("%").append(userInput).append("%")
    }

    private Product buildProduct(SearchProductView it){
        return new Product(Id : it.Id,
                title:it.title,
                imageLocation:it.imageLocation,
                brand:it.brand,
                name:it.name,
                quantity:it.quantity,
                currentPrice:it.currentPrice,
                usualPrice:it.usualPrice,
                purchasedPrice:it.purchasedPrice,
                availableQuantity:it.availableQuantity,
                productCategory:it.productCategory,
                subCategory:it.subCategory,
                subscriptionAvailability:it.subscriptionAvailability,
                productDetail:it.productDetail,
                measurement:it.measurement,
                subscriptionPrice:it.subscriptionPrice)
    }

    private ProductCategory buildProductCategory(SearchProductView it){
        return new ProductCategory(Id:it.Id,
                name:it.name,
                imageLocation:it.imageLocation,
                subCategory:it.subCategory)
    }
}
