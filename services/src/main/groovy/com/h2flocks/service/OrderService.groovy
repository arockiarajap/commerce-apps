package com.h2flocks.service

import com.h2flocks.data.OrderRespository
import com.h2flocks.data.ProductRepository
import com.h2flocks.dto.Cart
import com.h2flocks.dto.MyOrderLine
import com.h2flocks.dto.MyOrders
import com.h2flocks.entity.Order
import com.h2flocks.entity.OrderLineItem
import com.h2flocks.entity.OrderSource
import com.h2flocks.entity.OrderStatus
import com.h2flocks.entity.Product
import com.h2flocks.entity.Subscription
import com.h2flocks.entity.SubscriptionStatus
import com.h2flocks.entity.UserProfile
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class OrderService {

    Logger logger= LoggerFactory.getLogger(OrderService.class)

    @Autowired
    OrderRespository orderRespository

    @Autowired
    ProfileService profileService

    @Autowired
    ProductRepository productRepository

    @Autowired
    PaymentService paymentService

    @Autowired
    AccountingService accountingService

    OrderStatus saveOrder(String mobile,Cart cart,boolean isFinal,String deliveryAt){
        try{
            Date deliveryDate = Date.parse("yyyy-MM-dd", deliveryAt)
            UserProfile userProfile=profileService.getUserprofile(mobile)
            Order order=new Order(
                    name: cart.count+" cart items",
                    status: isFinal?OrderStatus.ORDERED:OrderStatus.YET_TO_BE_ORDERED,
                    serviceCharge: 2,
                    subTotal: cart.subtotal,
                    deliveryDate: deliveryDate,
                    profileId: userProfile.id,
                    orderedDate: new Date(),
                    createdAt: new Date(),
                    modifiedAt: new Date()
            )

            //add line items
            cart.items.each {
                Product product=productRepository.findById(it.skuId).get()
                order.orderLines.add(new OrderLineItem(
                        productId :it.skuId,
                        quantity:it.quantity,
                        soldAt:new Date(),
                        margin:product.currentPrice-product.purchasedPrice,
                        purchasedPrice:product.purchasedPrice,
                        soldPrice:product.currentPrice,
                        savings:product.usualPrice-product.currentPrice)
                )
            }

            //find savings with off service charge
            order.savings=order.serviceCharge
            order.serviceCharge=0
            order.orderLines.each {
                order.savings+=it.getSavings()
            }

            //find Total
            order.total=order.subTotal+order.serviceCharge

            //set source
            order.orderSource= OrderSource.CART

            //save the order
            order=orderRespository.save(order)

            //payment for the order and update the order as paid
            if(userProfile.isCreditAllowed || accountingService.getAccountBalance(userProfile)>=order.total){
                accountingService.makeOrder(userProfile,order,true)
                order.isPaid=true
            }
            else{
                order.status=paymentService.payOrder(mobile,order.id,order)?OrderStatus.ORDERED:OrderStatus.FAILED
                if(order.status==OrderStatus.ORDERED)
                    order.isPaid=true
                orderRespository.save(order)
            }

            //send notification

            //reduce the product availability
            order.orderLines.each {
                Product product=productRepository.findById(it.productId).get()
                product.availableQuantity-=it.quantity
                productRepository.save(product)
            }

            return order.status

        }
        catch(Exception ex){
            logger.info("The error occured while saving the order $ex for the cart $cart")
            return OrderStatus.FAILED
        }
    }

    OrderStatus subscriptionOrder(Subscription subscription, Date deliveryDate){
        try{

            //No order can be created if modification ends today
            if(subscription.status==SubscriptionStatus.MODIFIED && subscription.pausedSubscription.endDate==new Date())
                return OrderStatus.FAILED

            UserProfile userProfile=profileService.getProfileRepository().findById(subscription.profileId).get()

            Order order=new Order(
                    name: subscription.products.size()+" subscription items",
                    status: OrderStatus.ORDERED,
                    serviceCharge: 2,
                    subTotal: subscription.subTotal,
                    deliveryDate: deliveryDate,
                    profileId: subscription.profileId,
                    orderedDate: new Date(),
                    createdAt: new Date(),
                    modifiedAt: new Date()
            )

            //add line items
            if(subscription.status== SubscriptionStatus.ACTIVE)
                subscription.products.each {
                    Product product=productRepository.findById(it.productId).get()
                    order.orderLines.add(new OrderLineItem(
                            productId :it.productId,
                            quantity:it.quantity,
                            soldAt:new Date(),
                            margin:product.currentPrice-product.purchasedPrice,
                            purchasedPrice:product.purchasedPrice,
                            soldPrice:product.currentPrice,
                            savings:product.usualPrice-product.currentPrice)
                    )
                }
            else if(subscription.status==SubscriptionStatus.MODIFIED)
                subscription.modifiedSubscription.modifiedProducts.each {
                    Product product=productRepository.findById(it.productId).get()
                    order.orderLines.add(new OrderLineItem(
                            productId :it.productId,
                            quantity:it.quantity,
                            soldAt:new Date(),
                            margin:product.currentPrice-product.purchasedPrice,
                            purchasedPrice:product.purchasedPrice,
                            soldPrice:product.currentPrice,
                            savings:product.usualPrice-product.currentPrice)
                    )
                }

            //find savings with off service charge
            order.savings=order.serviceCharge
            order.serviceCharge=0
            order.orderLines.each {
                order.savings+=it.getSavings()
            }

            //find Total
            order.total=order.subTotal+order.serviceCharge

            //set source
            order.orderSource= OrderSource.SUBSCRIPTION
            order.sourceSubscriptionId=subscription.Id

            //save the order
            order=orderRespository.save(order)

            //payment for the order and update the order as paid
            if(userProfile.isCreditAllowed || accountingService.getAccountBalance(userProfile)>=order.total){
                accountingService.makeOrder(userProfile,order,true)
                order.isPaid=true
            }
            else{
                //send message to recharge
                order.status=OrderStatus.FAILED
            }

            //send notification

            //reduce the product availability
            order.orderLines.each {
                Product product=productRepository.findById(it.productId).get()
                product.availableQuantity-=it.quantity
                productRepository.save(product)
            }

            return order.status

        }
        catch(Exception ex){
            logger.info("The error occured while saving the order $ex for the subscription $subscription")
            return OrderStatus.FAILED
        }
    }

    Order updateOrder(String mobile, String orderId,OrderStatus updateStatus){
        try{
            Order order=orderRespository.findById(orderId).get()
            if(order && profileService.getUserprofile(mobile)){
                logger.info("The order # $orderId is been updated to, $updateStatus")
                order.status=updateStatus
                order.modifiedAt=new Date()
                orderRespository.save(order)
                return order
            }
            return null
        }
        catch(Exception ex){
            logger.info("The error occured while saving the order $ex")
            return null
        }
    }

    Order cancelOrder(String mobile, String orderId){
        try{
            Order order=orderRespository.findById(orderId).get()
            if(order && profileService.getUserprofile(mobile)){
                UserProfile userProfile=profileService.getUserprofile(mobile)
                if(accountingService.refundOnCancelOrder(mobile,order,userProfile.id))
                    return this.updateOrder(mobile,orderId,OrderStatus.REFUNDED)
                else
                    return this.updateOrder(mobile,orderId,OrderStatus.CANCELLED)
            }
            return null
        }catch(Exception ex){
            logger.info("The error occured while cancelling the order $ex")
            return null
        }
    }

    List<Order> cancelSubscriptionOrderIfMatches(Subscription subscription){
        try{
            Iterable<Order> orders=orderRespository.findBySubscriptionId(subscription.Id)
            String mobile=profileService.getProfileRepository().findById(subscription.profileId)
            orders.each {
                if(it.orderedDate==subscription.lastOrderedDate && it.status!=OrderStatus.CANCELLED)
                    this.cancelOrder(mobile,it.id)
            }
            orders.asList()
        }catch(Exception ex){
            logger.info("The error occured while cancelling the subscription order id:${subscription.Id} $ex")
            return null
        }
    }

    MyOrders myOrders(String mobile, String dateAt){
        try{
            String ordersAt = Date.parse("EEE-dd-MMM-yyyy", dateAt).format('yyyy-MM-dd')
            MyOrders myOrders=new MyOrders(
                    mobile: mobile,
                    date:dateAt
            )
            orderRespository.findByProfileId(ordersAt,profileService.getUserprofile(mobile).id).each {
                MyOrderLine myOrderLine=new MyOrderLine(order: it)
                myOrderLine.setOrderTitle(it)
                myOrderLine.setOrderStatus(it.status)
                it.orderLines.each {
                    myOrderLine.products.put(it.productId,productRepository.findById(it.productId).get())
                }
                myOrders.orders.add(myOrderLine)
            }
            return myOrders
        }
        catch(Exception ex){
            logger.info("The error occured while listing my orders $ex")
            return []
        }
    }

    List<Order> refundCancelledOrders(){
        try{
            List<Order> myOrders=[]
            orderRespository.findByStatus(OrderStatus.CANCELLED).each {
                UserProfile profile=profileService.getProfileRepository().findById(it.profileId).get()
                if(accountingService.refundOnCancelOrder(profile.mobile,it,profile.id))
                    this.updateOrder(profile.mobile,it.id,OrderStatus.REFUNDED)
                myOrders.push(it)
            }
            return myOrders
        }
        catch(Exception ex){
            logger.info("The error occured while listing my orders $ex")
            return []
        }
    }

    MyOrders getOrder(String orderId){
        try{
            Order order=orderRespository.findById(orderId).get()
            MyOrderLine myOrderLine=new MyOrderLine(order:order)
            myOrderLine.setOrderTitle(order)
            myOrderLine.setOrderStatus(order.status)
            order.orderLines.each {
                myOrderLine.products.put(it.productId,productRepository.findById(it.productId).get())
            }

            new MyOrders(orders: [myOrderLine])
        }
        catch(Exception ex){
            logger.info("The error occured while getting the order $orderId, $ex")
            return []
        }
    }
}
