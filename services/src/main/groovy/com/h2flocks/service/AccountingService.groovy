package com.h2flocks.service

import com.h2flocks.data.AccountingRepository
import com.h2flocks.data.ProfileRepository
import com.h2flocks.dto.MonthlyAccounting
import com.h2flocks.dto.MyAccounting
import com.h2flocks.dto.MyAccountingSummary
import com.h2flocks.entity.AccountBookLine
import com.h2flocks.entity.AccountBookLineDetail
import com.h2flocks.entity.AccountingBook
import com.h2flocks.entity.Order
import com.h2flocks.entity.PaymentHistory
import com.h2flocks.entity.UserProfile
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import java.text.Format
import java.text.SimpleDateFormat

@Service
class AccountingService {

    Logger logger= LoggerFactory.getLogger(AccountingService.class)

    Format MonthYearFormat = new SimpleDateFormat("MMMM yyyy")

    Format PaymentHistoryFormat = new SimpleDateFormat("MMM dd,yyyy")

    @Autowired
    AccountingRepository accountingRepository

    @Autowired
    PaymentService paymentService

    @Autowired
    ProfileRepository profileRepository

    boolean makeOrder(UserProfile profile,Order order,boolean isCredit){
        try{
            AccountingBook accountBook=accountingRepository.findByProfileId(profile.id)[0]
            AccountBookLine accountBookLine=new AccountBookLine(
                    eventDate: order.orderedDate,
                    debit: order.total,
                    credit: 0,
                    balance: accountBook?accountBook.balance-order.total:(isCredit?-order.total:0),
                    detail: new AccountBookLineDetail(
                            deliveryDate: order.deliveryDate,
                            remarks: "Delivery of Product",
                            serviceCharge: order.serviceCharge,
                            savings: order.savings
                    )
            )

            if(accountBook){
                accountBook.balance=accountBookLine.balance
                accountBook.accountBookLines.add(accountBookLine)
            }
            else{
                accountBook=new AccountingBook(
                        profileId: profile.id,
                        balance: accountBookLine.balance,
                        paymentHistories: [],
                        accountBookLines: [accountBookLine],
                        createdAt: new Date()
                )
            }
            accountBook.modifiedAt=new Date()
            accountingRepository.save(accountBook)!=null
        }catch(Exception e){
            logger.error("The error occurred on making the order adjustment : ${e.printStackTrace()}")
        }
    }

    Float getAccountBalance(UserProfile profile){
        try{
            AccountingBook accountingBook=accountingRepository.findByProfileId(profile.id)[0]
            accountingBook?accountingBook.balance:0.0
        }catch(Exception e){
            logger.error("The error occurred on getting the balance retrieval, mobile : ${profile.mobile} : ${e.printStackTrace()}")
        }
    }

    MyAccounting getUserAccounting(String mobile){
        try{
            UserProfile userProfile=profileRepository.findByMobile(mobile)[0]
            AccountingBook systemAccountBok=accountingRepository.findByProfileId(userProfile.id)[0]

            boolean isNewBook=systemAccountBok==null
            if(isNewBook)
                systemAccountBok=new AccountingBook(balance: 0.0)

            Map<String,List<PaymentHistory>> paymentHistories=[:]
            systemAccountBok.paymentHistories.each {
                String key=MonthYearFormat.format(it.paymentDate)
                paymentHistories.get(key)?paymentHistories.get(key).add(it):paymentHistories.put(key,[it])
            }

            Map<String,List<AccountBookLine>> accountBookLines=[:]
            systemAccountBok.accountBookLines.each {
                String key=MonthYearFormat.format(it.getEventDate())
                accountBookLines.get(key)?accountBookLines.get(key).add(it):accountBookLines.put(key,[it])
            }

            Map<String,MonthlyAccounting> monthlyAccounting=[:]
            accountBookLines.keySet().each {
                monthlyAccounting.put(it,new MonthlyAccounting(
                        bookLines: accountBookLines.get(it),
                        openingBalance: accountBookLines.get(it).first().balance,
                        closingBalance: accountBookLines.get(it).last().balance
                ))
            }

            MyAccountingSummary myAccountingSummary=new MyAccountingSummary(
                    lastPaidAmount: isNewBook?0.0:systemAccountBok.paymentHistories.last().paidAmount,
                    balanceAfterLastPayment: isNewBook?0.0:systemAccountBok.paymentHistories.last().balance,
                    billsSinceLastPayment: systemAccountBok.balance,
                    monthlyAverageExpense: isNewBook?0.0:this.monthlyAverageExpense(accountBookLines)
            )

            return new MyAccounting(
                    balance: systemAccountBok.balance,
                    myAccountingSummary:myAccountingSummary,
                    paymentHistories:paymentHistories.sort {it.key},
                    monthlyAccounting: monthlyAccounting.sort {it.key}
            )

        }catch(Exception e){
            logger.error("The error occurred on getting the account book retrieval, mobile : ${mobile} : ${e.printStackTrace()}")
        }
    }

    MyAccounting payMoney(String mobile,Float amount,String transactionType){
        try{
            UserProfile profile=profileRepository.findByMobile(mobile)[0]
            AccountingBook accountBook=accountingRepository.findByProfileId(profile.id)[0]
            AccountBookLine accountBookLine=new AccountBookLine(
                    eventDate: new Date(),
                    debit: 0,
                    credit: amount,
                    balance: accountBook?accountBook.balance+amount:amount,
                    detail: new AccountBookLineDetail(
                            deliveryDate: new Date(),
                            remarks: "Recharge Account book",
                            serviceCharge: 0,
                            savings: 0
                    )
            )
            PaymentHistory paymentHistory=new PaymentHistory(
                    name: 'Paid on '+PaymentHistoryFormat.format(new Date()),
                    transactionType: transactionType,
                    paymentDate: new Date(),
                    status: 'Failed',
                    paidAmount: amount,
                    balance: accountBookLine.balance
            )
            if(paymentService.payAccountMoney(profile.id+new Date(),mobile,profile,amount)){
                paymentHistory.status="Success"
            }
            else
                accountBookLine.balance-=amount

            if(accountBook){
                accountBook.balance=accountBookLine.balance
                accountBook.accountBookLines.add(accountBookLine)
                accountBook.paymentHistories.add(paymentHistory)
            }
            else{
                accountBook=new AccountingBook(
                        profileId: profile.id,
                        balance: accountBookLine.balance,
                        paymentHistories: [paymentHistory],
                        accountBookLines: [accountBookLine],
                        createdAt: new Date()
                )
            }
            accountBook.modifiedAt=new Date()
            accountingRepository.save(accountBook)
            return this.getUserAccounting(mobile)
        }catch(Exception e){
            logger.error("The error occurred on paying money for account book, mobile : ${mobile} : ${e.printStackTrace()}")
        }
    }

    boolean refundOnCancelOrder(String mobile,Order order,String profileId){
        try{
            AccountingBook accountBook=accountingRepository.findByProfileId(profileId)[0]
            Float closedBalance=accountBook?accountBook.balance:0.0;
            AccountBookLine accountBookLine=new AccountBookLine(
                    eventDate: order.orderedDate,
                    debit: 0,
                    credit: order.total,
                    balance: closedBalance+order.total,
                    detail: new AccountBookLineDetail(
                            deliveryDate: order.deliveryDate,
                            remarks: "Refund of order",
                            serviceCharge: 0,
                            savings: 0
                    )
            )

            if(accountBook){
                accountBook.balance=accountBookLine.balance
                accountBook.accountBookLines.add(accountBookLine)
            }
            else{
                accountBook=new AccountingBook(
                        profileId: profileId,
                        balance: accountBookLine.balance,
                        paymentHistories: [],
                        accountBookLines: [accountBookLine],
                        createdAt: new Date()
                )
            }
            accountBook.modifiedAt=new Date()
            accountingRepository.save(accountBook)!=null
        }catch(Exception e){
            logger.error("The error occurred on making the order adjustment : ${e.printStackTrace()}")
        }
    }

    private Float monthlyAverageExpense(Map<String,List<AccountBookLine>> accountBookLines){
        List<Float> monthlyExpenses=[]
        accountBookLines.each {
            Float monthExpenses=0
            it.value.each {
                monthExpenses=+it.getDebit()
            }
            monthlyExpenses.add(monthExpenses)
        }
        monthlyExpenses.sum()/monthlyExpenses.size()
    }
}
