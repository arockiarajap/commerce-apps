package com.h2flocks.service

import com.h2flocks.data.ProductCategoryRepository
import com.h2flocks.data.ProductRepository
import com.h2flocks.entity.Product
import com.h2flocks.entity.ProductCategory
import com.h2flocks.entity.ProductDetail
import com.h2flocks.utils.H2FUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class ProductService {

    Logger logger= LoggerFactory.getLogger(ProductService.class)

    @Value('${product.category.image}')
    String categoryLocation

    @Value('${product.image}')
    String productLocation

    @Autowired
    ProductCategoryRepository productCategoryRepository

    @Autowired
    ProductRepository productRepository

    @Autowired
    H2FUtils h2FUtils

    /***
     * The category of products will be unique by name
     * @param name
     * @param categoryImage
     * @return
     */
    boolean createOrModifyCategory(String name, MultipartFile categoryImage, List<String> subCategory){
        try{
            ProductCategory existing=this.getCategory(name),productCategory
            if(existing){
                productCategory=existing
            }else{
                productCategory=new ProductCategory(createdAt: new Date())
            }
            productCategory.name=name
            productCategory.imageLocation=h2FUtils.saveFile(categoryLocation,categoryImage)
            productCategory.subCategory=subCategory
            productCategory.modifiedAt=new Date()
            productCategoryRepository.save(productCategory)!=null
        }catch(Exception ex){
            logger.info("The category couldn't be saved $ex")
            return false
        }
    }

    boolean createOrModifyProduct(String id,String title,MultipartFile productImage,String brand,String name,Integer quantity,Float currentPrice,Float usualPrice,
                          Float purchasedPrice,Integer availableQuantity,String productCategory,List<String> subCategory,boolean subscriptionAvailability,
                          ProductDetail productDetail,String measurement,Float subscriptionPrice){
        try{
            Product product
            if(id){
                product=productRepository.findById(id)
            }else {
                product=new Product(createdAt: new Date())
            }
            product.title=title
            product.imageLocation=h2FUtils.saveFile(productLocation,productImage)
            product.brand=brand
            product.name=name
            product.quantity=quantity
            product.currentPrice=currentPrice
            product.usualPrice=usualPrice
            product.purchasedPrice=purchasedPrice
            product.availableQuantity=product.availableQuantity+availableQuantity
            product.productCategory=productCategory
            product.subCategory=subCategory
            product.subscriptionAvailability=subscriptionAvailability
            product.productDetail=productDetail
            product.measurement=measurement
            product.subscriptionPrice=subscriptionPrice
            product.modifiedAt=new Date()
            productRepository.save(product)!=null
        }
        catch(Exception ex){
            logger.info("The product couldn't be saved $ex")
            return false
        }
    }

    List<ProductCategory> getAllCategories(){
        try{
            Iterable<ProductCategory> productCategories=productCategoryRepository.findAll()
            return productCategories.toList()
        }
        catch(Exception ex){
            logger.info("The error occured while getting the product categories $ex")
            return []
        }
    }

    List<Product> getAlProducts(){
        try{
            Iterable<Product> productList=productRepository.findAll()
            return productList.toList()
        }
        catch(Exception ex){
            logger.info("The error occured while getting the products $ex")
            return []
        }
    }

    Map<String,Object> getCategoryProducts(String category){
        try{
            boolean isIdBased=category.matches("[0-9]+")
            Map<String,Object> productsByCategory=[:]
            ProductCategory belongedCategory=isIdBased?productCategoryRepository.findById(category).get():productCategoryRepository.findByName(category)[0]
            productsByCategory.put('category',belongedCategory)
            List<Product> products=productRepository.findByCategory(belongedCategory.getId())
            productsByCategory.put('products',products)
            return productsByCategory
        }
        catch(Exception ex){
            logger.info("The error occured while getting the products $ex")
            return [:]
        }
    }

    ProductDetail getProductDescription(String name){
        try{
            return this.getProduct(name).productDetail
        }
        catch(Exception ex){
            logger.info("The error occured while getting the products $ex")
            return null
        }
    }

    List<Product> getProduct(String name){
        try{
            Iterable<Product> products=productRepository.findByName(name)
            return products.toList()
        }
        catch(Exception ex){
            logger.info("The error occured while getting the products $ex")
            return null
        }
    }

    /**
     * Since the name is unique, once category would be there always
     * @param name
     * @return
     */
    ProductCategory getCategory(String name){
        try{
            Iterable<ProductCategory> productCategories=productCategoryRepository.findByName(name)
            return productCategories[0]
        }
        catch(Exception ex){
            logger.info("The error occured while getting the products $ex")
            return null
        }
    }

    boolean deleteProduct(String id){
        try{
            productRepository.deleteById(id)
            return true
        }
        catch(Exception ex){
            logger.info("The error occured while getting the products $ex")
            return false
        }
    }

    boolean deleteCategory(String id){
        try{
            productCategoryRepository.deleteById(id)
            return true
        }
        catch(Exception ex){
            logger.info("The error occured while getting the products $ex")
            return false
        }
    }
}
