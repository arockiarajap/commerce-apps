package com.h2flocks.service

import com.fasterxml.jackson.databind.util.JSONPObject
import com.h2flocks.data.ProfileRepository
import com.h2flocks.entity.OtpResponse
import com.h2flocks.entity.UserProfile
import groovy.json.JsonSlurper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class ProfileService {

    Logger logger= LoggerFactory.getLogger(ProfileService.class)

    @Value('${otp.send}')
    String sendOtpService

    @Value('${otp.verify}')
    String verifyOtpService

    @Autowired
    ProfileRepository profileRepository

    @Autowired
    @Qualifier("otpRestTemplate")
    RestTemplate otpRestTemplate

    @Autowired
    @Qualifier("otpVerifyRestTemplate")
    RestTemplate otpVerifyRestTemplate

    @Autowired
    JsonSlurper jsonSlurper

    boolean createNewProfile(String phone){
        try{
            UserProfile userIfExisting=this.getUserprofile(phone),userProfile
            if(userIfExisting)
                userProfile=userIfExisting
            else
                userProfile=profileRepository.save(new UserProfile(mobile: phone,createdAt: new Date(),modifiedAt: new Date()))
            ResponseEntity<String> otpSendResponse=otpRestTemplate.getForEntity(sendOtpService.replace('phone_no',userProfile.mobile), String)
            logger.info("The 2factor response for phone: $phone, response : $otpSendResponse")
            def otpJsonObject =jsonSlurper.parseText(otpSendResponse.getBody().toString())
            OtpResponse otpResponse=new OtpResponse(Status: otpJsonObject['Status'],Details: otpJsonObject['Details'])
            userProfile.userOtp.otpSendResponse.add(otpResponse)
            userProfile.mobileVerified=false
            userProfile.userOtp.sessionId=otpResponse.getDetails()
            userProfile.modifiedAt=new Date()
            profileRepository.save(userProfile)
            return otpResponse.getStatus()=="Success"
        }
        catch (Exception ex){
            logger.error("The issue occured while creating the new user $ex")
            return false
        }
    }

    boolean verifyOtp(String phone,String otp){
        try{
            UserProfile userProfile=this.getUserprofile(phone)
            if(this.isOTPSentActually(userProfile)){
                userProfile.userOtp.userOtp=otp
                ResponseEntity<String> otpVerifyResponse=otpVerifyRestTemplate.getForEntity(verifyOtpService.replace('otp_session',userProfile.userOtp.sessionId).replace('user_otp',otp), String)
                logger.info("The 2factor response for phone otp: $phone, response : $otpVerifyResponse")
                def otpJsonObject =jsonSlurper.parseText(otpVerifyResponse.getBody())
                OtpResponse otpResponse=new OtpResponse(Status: otpJsonObject['Status'],Details: otpJsonObject['Details'])
                userProfile.userOtp.otpVerifyResponse.add(otpResponse)
                if(otpResponse.getStatus()=="Success")
                    userProfile.mobileVerified=true
                userProfile.modifiedAt=new Date()
                profileRepository.save(userProfile)
                return otpResponse.getStatus()=="Success"
            }
            else return false
        }
        catch (Exception ex){
            logger.error("The issue occured while veryfying the otp, $ex")
            return false
        }
    }

    boolean isVerified(String phone){
        try{
            UserProfile userProfile=this.getUserprofile(phone)
            return userProfile && userProfile.mobileVerified
        }
        catch (Exception ex){
            logger.error("The issue occured while authrizing user, $ex")
            return false
        }
    }

    boolean logout(String phone){
        try{
            UserProfile userProfile=this.getUserprofile(phone)
            if(this.isVerified(phone)){
                userProfile.mobileVerified=false
                userProfile.modifiedAt=new Date()
                profileRepository.save(userProfile)
                return true
            }
            else return false
        }
        catch (Exception ex){
            logger.error("The issue occured while logging out the user, $ex")
            return false
        }
    }


    UserProfile getUserprofile(String phone){
        Iterable<UserProfile> userProfiles=profileRepository.findByMobile(phone)
        return userProfiles[0]
    }

    List<UserProfile> findAllProfile(){
        Iterable<UserProfile> userProfiles=profileRepository.findAllProfile()
        List<UserProfile> profiles=[]
        userProfiles.each {profiles.add(it)}
        return profiles
    }

    private boolean isOTPSentActually(UserProfile userProfile){
        if(userProfile){
            int totalOtp=userProfile.userOtp.otpSendResponse.size()
            OtpResponse otpSendResponse=userProfile.userOtp.otpSendResponse[totalOtp-1]
            return otpSendResponse.Status=="Success"
        }
        else
            return false
    }

}
