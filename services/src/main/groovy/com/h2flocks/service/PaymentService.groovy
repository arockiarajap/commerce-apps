package com.h2flocks.service

import com.h2flocks.data.PaymentRespository
import com.h2flocks.entity.Order
import com.h2flocks.entity.Payment
import com.h2flocks.entity.PaymentResponse
import com.h2flocks.entity.UserProfile
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

@Service
class PaymentService {

    Logger logger= LoggerFactory.getLogger(PaymentService.class)

    @Value('${initiate.payment}')
    String paymentUrl

    @Value('${payment.key}')
    String paymentKey

    @Value('${payment.salt}')
    String paymentSalt

    @Autowired
    @Qualifier("initiatePaymentRestTemplate")
    RestTemplate initiatePaymentRestTemplate

    @Autowired
    PaymentRespository paymentRespository

    boolean payOrder(String mobile, String transactionId,Order order){
        logger.info("The payment is starting to request for mobile : $mobile, order id : $transactionId")
        try{
            HttpHeaders headers = new HttpHeaders()
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED)
            String hash="$paymentKey|$transactionId|${order.total}|${order.orderLines[0].productId}|raja|arockiarajap@gmail.com| | | | | | | | | | |$paymentSalt"
            MultiValueMap<String, Object> request = new LinkedMultiValueMap<String, Object>()
            request.add("hash", get_SHA_512_Hash("SHA-512", hash))
            request.add("key", paymentKey)
            request.add("amount",order.total)
            request.add("email","arockiarajap@gmail.com")
            request.add("phone",mobile)
            request.add("firstname","raja")
            request.add("surl","http://localhost:8080/ForYou/index.html#success")
            request.add("furl","http://localhost:8080/ForYou/index.html#failure")
            request.add("txnid",transactionId)
            request.add("productinfo",order.orderLines[0].productId)

            ResponseEntity<PaymentResponse> response = initiatePaymentRestTemplate.postForEntity(paymentUrl,request,PaymentResponse.class)
            paymentRespository.save(
                    new Payment(
                            mobile : mobile,
                            request: request,
                            isOrder: true,
                            isSubscription: false,
                            isAccountBookPayment: false,
                            orderId: order.id,
                            createdAt: new Date(),
                            modifiedAt: new Date(),
                            response: response.getBody()
                    )
            )
            return response.getBody().status=="1"
        }catch(Exception ex){
            logger.error("The error occured during the payment processing ${ex.printStackTrace()}")
            return false
        }
    }

    boolean payAccountMoney(String transactionId,String mobile, UserProfile profile,Float amount){
        logger.info("The payment is starting to request for mobile in the account book : $mobile, order id : $transactionId")
        try{
            HttpHeaders headers = new HttpHeaders()
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED)
            String hash="$paymentKey|$transactionId|$amount|${profile.id}|raja|arockiarajap@gmail.com| | | | | | | | | | |$paymentSalt"
            MultiValueMap<String, Object> request = new LinkedMultiValueMap<String, Object>()
            request.add("hash", get_SHA_512_Hash("SHA-512", hash))
            request.add("key", paymentKey)
            request.add("amount",amount)
            request.add("email",profile.email)
            request.add("phone",mobile)
            request.add("firstname",profile.firstName)
            request.add("surl","http://localhost:8080/ForYou/index.html#success")
            request.add("furl","http://localhost:8080/ForYou/index.html#failure")
            request.add("txnid",transactionId)
            request.add("productinfo",profile.id)

            ResponseEntity<PaymentResponse> response = initiatePaymentRestTemplate.postForEntity(paymentUrl,request,PaymentResponse.class)
            paymentRespository.save(
                    new Payment(
                            mobile : mobile,
                            request: request,
                            isOrder: false,
                            isSubscription: false,
                            isAccountBookPayment: true,
                            createdAt: new Date(),
                            modifiedAt: new Date(),
                            response: response.getBody()
                    )
            )
            return response.getBody().status=="1"
        }catch(Exception ex){
            logger.error("The error occured during the payment processing in the account book ${ex.printStackTrace()}")
            return false
        }
    }

    private static String get_SHA_512_Hash(String type, String str){
        byte[] hashseq = str.getBytes()
        StringBuffer hexString = new StringBuffer()
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type)
            algorithm.reset()
            algorithm.update(hashseq)
            byte[] messageDigest = algorithm.digest()
            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i])
                if (hex.length() == 1) {
                    hexString.append("0")
                }
                hexString.append(hex)
            }
        } catch (NoSuchAlgorithmException nsae) {
            throw nsae
        }
        return hexString.toString()
    }
}
